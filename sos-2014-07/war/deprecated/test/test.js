function getURLParameter(sParam){
	var sPageURL = window.location.hash.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
			return sParameterName[1];
	}
}
function exportTableToCSV($table, filename) {

	var $rows = $table.find('tr:has(td)'),

	            // Temporary delimiter characters unlikely to be typed by keyboard
	            // This is to avoid accidentally splitting the actual contents
	            tmpColDelim = String.fromCharCode(11), // vertical tab character
	            tmpRowDelim = String.fromCharCode(0), // null character

	            // actual delimiter characters for CSV format
	            colDelim = '","',
	            rowDelim = '"\r\n"',

	            // Grab text from table into CSV formatted string
	            csv = '"' + $rows.map(function (i, row) {
	            	var $row = $(row),
	            	$cols = $row.find('td');

	            	return $cols.map(function (j, col) {
	            		var $col = $(col),
	            		text = $col.text();

	                    return text.replace('"', '""'); // escape double quotes

	                }).get().join(tmpColDelim);

	            }).get().join(tmpRowDelim)
	            .split(tmpRowDelim).join(rowDelim)
	            .split(tmpColDelim).join(colDelim) + '"',

	            // Data URI
	            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

	            $(this)
	            .attr({
	            	'download': filename,
	            	'href': csvData,
	            	'target': '_blank'
	            });
	        }
	        $(document)
	        .ready(
	        	function () {
                    //Prepare jTable
                    $('#birdTable')
                    .jtable({
                    	title: 'Birds',
                    	ajaxSettings: {
                    		type: 'POST',
                    		dataType: 'json'
                    	},
                        	//paging: true,
                           // pageSize: 10,
                           sorting: true,
                           multiSorting: true,
                           defaultSorting: 'species ASC',
                            selecting: true, //Enable selecting
                            multiselect: false, //Allow multiple selecting
                            selectingCheckboxes: true, //Show checkboxes on first column
                            selectOnRowClick: true, //Enable this to only select using checkboxes
                            recordsLoaded: function(event, data) {
                            	$('.jtable-data-row').click(function() {
                                    // MAP Configuration
                                    var zoom = 15;
                                    var mapWidth = 400; 
                                    var mapHeight = 400;
                                    var markerColor = "Red";
                                    var markerLabel = "Watching";
                                    var mapType = "satellite";
                                    // END MAP Configuration
                                    var $selectedRows = $('#birdTable').jtable('selectedRows');
                                    var lat = 0; var lng=0; 
                                    $selectedRows.each(function () {
                                    	var record = $(this).data('record');
                                    	lat = record.latitude;
                                    	lng =record.longitude;

                                    });

                                    var token = getURLParameter("access_token");
                                    console.log("Token: "+token);
                                    var urlLoc = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=false";
                                    var urlMap = "http://maps.google.com/maps/api/staticmap?" + "center=" + lat + "," + lng + "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&maptype="+mapType+"&markers=color:" + markerColor + "|label:" + markerLabel + "|" + lat + "," + lng+"&sensor=" + "false"+"&token="+token;
                                    var urlWea = "http://api.openweathermap.org/data/2.5/find?lat="+lat+"&lon="+lng+"&units=metric";
                                    console.log(urlLoc);
                                    console.log(urlMap);
                                    console.log(urlWea);

                                    var img = $("<img>");
                                    img.attr( { src: urlMap} );
                                    $("#map").html(img);
                                    console.log("Lat: "+lat+" Long: "+lng);

                                    $.getJSON(urlLoc, function( data ) {
                                    	var address = data["results"][0]["formatted_address"]
                                    	console.log(address);
                                    	$("#address").html(address);
                                    });
                                    
                                    $.getJSON(urlWea, function( data ) {

                                    	var weather = data["list"][0]["main"];
                                    	var temp = weather["temp"];
                                    	var hum = weather["humidity"];
                                    	var press = weather["pressure"];
                                    	var tempMin = weather["temp_min"];
                                    	var tempMax = weather["temp_max"];

                                    	console.log(temp+" "+hum+" "+tempMin+" "+tempMax);

                                    	$("#temp").html("Temperature: "+temp+" ºC");
                                    	$("#hum").html("Humidity: "+hum+" %");
                                    	$("#press").html("Pressure "+press+" mbar");
                                        //$("#tempMin").html(tempMin+" ºC");
                                        //$("#tempMax").html(tempMax+" ºC");
                                    });
                                });
},

actions: {
	listAction: function (postData, jtParams) {
                                    //console.log("Executing GET");
                                    return $.Deferred(function ($dfd) {
                                    	$.ajax({
                                    		url: apiPath + '/watchings',
                                    		type: 'GET',
                                    		dataType: 'json',
                                    		success: function (data) {
                                                // Important note: this widget requires a JSON with "Result":"OK", "Records": [{......}]
                                                var dataFromGet = JSON.stringify(data);
                                                var dataFromGetTrimmed = dataFromGet.substring(2, dataFromGet.length - 2);
                                                var composedJson = '{"Result":"OK", "Records":[{' + dataFromGetTrimmed + '}]}';
                                                var newData = jQuery.parseJSON(composedJson);
                                                //console.log(newData);
                                                $dfd.resolve(newData);
                                            },
                                            error: function () {
                                            	$dfd.reject();
                                            }
                                        });
});
},


                                // TODO: envia jsonp en vex de json normal y server no lo pilla
                                createAction: function (postData, jtParams) {
                                	return $.Deferred(function ($dfd) {
                                		var json = "{";
                                		$.each(postData.split("&"), function (index, value) {
                                            //console.log(value);
                                            //var param = value.replace("=",":");
                                            var splitted = value.split("=");
                                            var atrib = '"' + splitted[0] + '"';
                                            splitted[1] = splitted[1].replace(/\%2B/g,' ');
                                            splitted[1] = splitted[1].replace(/\+/g,' ');
                                            var val = '"' + splitted[1] + '"';
                                            var param = atrib + ":" + val;
                                            json += param + ", ";

                                        });
                                		json = json.slice(0, -2);
                                		json += "}";
                                        //console.log(json);

                                        //

                                        $.ajax({
                                        	url: apiPath + '/watchings',
                                        	type: 'POST',
                                        	data: json,
                                        	dataType: 'json',
                                        	success: function (data) {
                                                // Important note: this widget requires a JSON with "Result":"OK", "Records": [{......}]

                                                var composedJson = '{"Result":"OK", "Record":' + json + '}';
                                                var newData = jQuery.parseJSON(composedJson);
                                                //console.log(newData);
                                                $dfd.resolve(newData);
                                            },
                                            error: function () {
                                            	$dfd.reject();
                                            }
                                        });

});
},

                                // TODO: implementar con conceptos de arriba
                                updateAction: function (postData, jtParams) {
                                	return $.Deferred(function ($dfd) {
                                		var json = "{";
                                		var id = postData.split("&")[0].split("=")[1];
                                		$.each(postData.split("&"), function (index, value) {
                                            //console.log(value);
                                            //var param = value.replace("=",":");
                                            var splitted = value.split("=");
                                            var atrib = splitted[0];
                                            var val = "";
                                            if (atrib == "latitude" || atrib == "longitude") {
                                            	val = splitted[1];
                                            } else {
                                            	splitted[1] = splitted[1].replace(/\%2B/g,' ');
                                            	splitted[1] = splitted[1].replace(/\+/g,' ');
                                            	val = '"' + splitted[1] + '"';
                                            }

                                            atrib = '"' + splitted[0] + '"';
                                            var param = atrib + ":" + val;

                                            json += param + ", ";

                                        });
json = json.slice(0, -2);
json += "}";
                                        //console.log(json);

                                        //

                                        $.ajax({
                                        	url: apiPath + '/watchings/' + id,
                                        	type: 'PUT',
                                        	data: json,
                                        	dataType: 'json',
                                        	success: function (data) {
                                                // Important note: this widget requires a JSON with "Result":"OK", "Records": [{......}]

                                                var composedJson = '{"Result":"OK", "Record":' + json + '}';
                                                var newData = jQuery.parseJSON(composedJson);
                                                //console.log(newData);
                                                $dfd.resolve(newData);
                                            },
                                            error: function () {
                                            	$dfd.reject();
                                            }
                                        });

});
},

deleteAction: function (postData, jtParams) {
                                    //console.log("Executing DELETE");
                                    //console.log(postData.id);
                                    return $.Deferred(function ($dfd) {
                                        //console.log(postData);
                                        $.ajax({
                                        	url: apiPath + '/watchings/' + postData.id,
                                        	type: 'DELETE',
                                        	dataType: 'json',
                                        	success: function (data) {
                                                // Important note: this widget requires a JSON with "Result":"OK", "Records": [{......}]
                                                var dataFromGet = JSON.stringify(data);
                                                var dataFromGetTrimmed = dataFromGet.substring(2, dataFromGet.length - 2);
                                                var composedJson = '{"Result":"OK", "Records":[{' + dataFromGetTrimmed + '}]}';
                                                var newData = jQuery.parseJSON(composedJson);
                                                //console.log(newData);
                                                $dfd.resolve(newData);
                                            },
                                            error: function () {
                                            	$dfd.reject();
                                            }
                                        });
});
}
},

toolbar: {
	items: [{
		icon: '/src/img/xls.gif',
		text: 'Export to CSV',
		click: function (event) {
			var csv = $('#birdTable').table2CSV({delivery:'value'});
                                    	 //It works great, but just in Chrome. Another way to do the same is needed
                                        //var dataUri = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
                                        //var filename = "data.csv"
                                        //$("<a download='" + filename + "' href='" + dataUri + "'></a>")[0].click();
                                        
                                        function download(strData, strFileName, strMimeType) {
                                        	var D = document,
                                        	a = D.createElement("a");
                                        	strMimeType= strMimeType || "application/octet-stream";


                                            if (navigator.msSaveBlob) { // IE10
                                            	return navigator.msSaveBlob(new Blob([strData], {type: strMimeType}), strFileName);
                                            } /* end if(navigator.msSaveBlob) */


                                            if ('download' in a) { //html5 A[download]
                                            	a.href = "data:" + strMimeType + "," + encodeURIComponent(strData);
                                            	a.setAttribute("download", strFileName);
                                            	a.innerHTML = "downloading...";
                                            	D.body.appendChild(a);
                                            	setTimeout(function() {
                                            		a.click();
                                            		D.body.removeChild(a);
                                            	}, 66);
                                            	return true;
                                            } /* end if('download' in a) */


                                            //do iframe dataURL download (old ch+FF):
                                            var f = D.createElement("iframe");
                                            D.body.appendChild(f);
                                            f.src = "data:" +  strMimeType   + "," + encodeURIComponent(strData);

                                            setTimeout(function() {
                                            	D.body.removeChild(f);
                                            }, 333);
                                            return true;
                                        } /* end download() */
                                        
                                        download(csv, "Watchings.csv", "text/csv");
                                    }
                                }, 
                                {
                                	icon: '/src/img/pdf.gif',
                                	text: 'Export to PDF',
                                	click: function () {
                                		var pdf = new jsPDF('l', 'pt', 'a3')


                                        // source can be HTML-formatted string, or a reference
                                        // to an actual DOM element from which the text will be scraped.
                                        ,
                                        source = $('#birdTable')[0]

                                            // we support special element handlers. Register them with jQuery-style
                                            // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
                                            // There is no support for any other type of selectors
                                            // (class, of compound) at this time.
                                            ,
                                            specialElementHandlers = {
                                                // element with id of "bypass" - jQuery style selector
                                                '.jtable-toolbar-item': function (element, renderer) {
                                                    // true = "handled elsewhere, bypass text extraction"
                                                    return false
                                                }
                                            }

                                            margins = {
                                            	top: 80,
                                            	bottom: 60,
                                            	left: 40,
                                            	width: 522
                                            };
                                        // all coords and widths are in jsPDF instance's declared units
                                        // 'inches' in this case
                                        pdf.fromHTML(
                                            source // HTML string or DOM elem ref.
                                            , margins.left // x coord
                                            , margins.top // y coord
                                            , {
                                                'width': margins.width // max width of content on PDF
                                                ,
                                                'elementHandlers': specialElementHandlers
                                            },
                                            function (dispose) {
                                                // dispose: object with X, Y of the last line add to the PDF
                                                //          this allow the insertion of new lines after html
                                                pdf.save('Watchings.pdf');
                                            },
                                            margins
                                            )
}
}]
},

fields: {

	GUI: {
		title: 'GUI',
		type: 'textarea',
		create: true,
		key: true,
		inputClass: 'validate[required]'
	},
	species: {
		title: 'Specie',
		type: 'textarea',
		inputClass: 'validate[required]'
	},
	commonName: {
		title: 'Common Name',
		type: 'textarea',
		inputClass: 'validate[required]'
	},
	watchSiteName: {
		title: 'Place',
		type: 'textarea',
		inputClass: 'validate[required]'
	},
	latitude: {
		title: 'Latitude',
		type: 'textarea',
		inputClass: 'validate[required,custom[number]]'
	},
	longitude: {
		title: 'Longitude',
		type: 'textarea',
		inputClass: 'validate[required,custom[number]]'
	},
	date: {
		title: 'Date',
		type: 'textarea',
		inputClass: 'validate[required]',
		type: 'date',
		displayFormat: 'yy-mm-dd'
	}



},

                          //Initialize validation logic when a form is created
                          formCreated: function (event, data) {
                          	data.form.validationEngine();
                          },
                            //Validate form when it is being submitted
                            formSubmitting: function (event, data) {
                            	return data.form.validationEngine('validate');
                            },
                            //Dispose validation logic when form is closed
                            formClosed: function (event, data) {
                            	data.form.validationEngine('hide');
                            	data.form.validationEngine('detach');
                            }

                        });
                    //Load person list from server
                    $('#birdTable')
                    .jtable('load');
                });
$('#DeleteAllButton').button().click(function () {
	var $selectedRows = $('#birdTable').jtable('selectedRows');
	$('#birdTable').jtable('deleteRows', $selectedRows);
});

$('#DeleteAllDatastoreButton').button().click(function () {
	$.ajax({
		url: apiPath + '/watchings',
		type: 'DELETE',
		success: function(result) {
			alertify.success("All rows have been successfully deleted");
		}
	});
});
