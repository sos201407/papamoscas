
function getMainCandidates(array_zone){
    // array_zone: Object {numero_partidos: "39", partido: Array[39]}
    var name_cand1 =  array_zone["partido"][0]["nombre"];
    var votes_cand1 =  array_zone["partido"][0]["votos_porciento"];
    var name_cand2 =  array_zone["partido"][1]["nombre"];
    var votes_cand2 =  array_zone["partido"][1]["votos_porciento"];

    return [[name_cand1,votes_cand1],[name_cand2,votes_cand2]];
  }

// getting province name by INE id

function getZoneName(zone){
  var res;
  switch (zone) {
   case 01:
   res = "Andalucía";
   break;
   case 02:
   res = "Aragón";
   break;
   case 03:
   res = "Asturias";
   break;
   case 04:
   res = "Baleares";
   break;
   case 05:
   res = "Canarias";
   break;
   case 06:
   res = "Cantabria";
   case 07:
   res = "Castilla y León";
   break;
   case 08:
   res = "Castilla - La Mancha";
   break;
   case 09:
   res = "Cataluña";
   break;
   case 10:
   res = "Valencia";
   break;
   case 11:
   res = "Extremadura";
   break;
   case 12:
   res = "Galicia";
   break;
   case 13:
   res = "Madrid";
   break;
   case 14:
   res = "Murcia";
   break;
   case 15:
   res = "Navarra";
   break;
   case 16:
   res = "País Vasco";
   break;
   case 17:
   res = "La Rioja";
   break;
   case 18:
   res = "Ceuta";
   break;
   case 19:
   res = "Melilla";
   break;
   default:
   res="Otro";
       //console.log("Another zone: "+ zone);

     }

   //console.log(res);
   return res;
 }


 function getRepresentativeCity(ccaa){


    // NOTE: we only consider the following mapping between cities and 'ccaa'.

/*
        :INE codes are used here:

01 - 41
02 - 50
03 - 33
04 - 35
05 - 38
06 - 39
07 - 47
08 - 45
09 - 08

10 - 46
11 - 06
12 - 15
13 - 28
14 - 30
15 - 31
16 - 20
17 - 26
18 - 51 
19 - 52

*/

var resCcaa;
switch (ccaa) {
 case 1:
 resCcaa = 41;
 break;
 case 2:
 resCcaa = 50;
 break;
 case 3:
 resCcaa = 33;
 break;
 case 4:
 resCcaa = 35;
 break;
 case 5:
 resCcaa = 38;
 break;
 case 6:
 resCcaa = 39;
 case 7:
 resCcaa = 47;
 break;
 case 8:
 resCcaa = 45;
 break;
 case 9:
 resCcaa = 08;
 break;
 case 10:
 resCcaa = 46;
 break;
 case 11:
 resCcaa = 06;
 break;
 case 12:
 resCcaa = 15;
 break;
 case 13:
 resCcaa = 28;
 break;
 case 14:
 resCcaa = 30;
 break;
 case 15:
 resCcaa = 31;
 break;
 case 16:
 resCcaa = 20;
 break;
 case 17:
 resCcaa = 26;
 break;
 case 18:
 resCcaa = 51;
 break;
 case 19:
 resCcaa = 52;
 break;
 default:
 res="Otro";
 //console.log("Another zone: "+ ccaa);

}
return resCcaa;

}
// getting results from each 'comunidad autonoma'

var arr = [];

for ( var i = 1; i <= 19; i++ ) {
  subArr = [];
  res = [];
  if(i<10)i="0"+i.toString();
  else i=i.toString();
    //console.log(i);
    var url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20xml%20where%20url%3D%22http%3A%2F%2Frsl00.epimg.net%2Felecciones%2F2014%2Feuropeas%2F"+i+"%2Findex.xml2%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";
    $.ajax({
      url: url,
      dataType: 'json',
      async: false,
      success: function(data) {
            // Example: Object {numero_partidos: "39", partido: Array[39]}
            //console.log(data);
            var array_zone = data["query"]["results"]["escrutinio_sitio"]["resultados"];
            var mainPercents = getMainCandidates(array_zone); //[cand1,cand2];
            subArr.push(mainPercents);
            subArr.push(i);
          }
        });
    arr.push(subArr)
  }
//console.log(arr);



var values_votesPP=[];
var values_votesPSOE=[];
$.each(arr,function(i,elem){
 key    = elem[1];

 if(elem[0][0][0]=="PP"){
         valuePP = elem[0][0][1]; //% A
         valuePSOE = elem[0][1][1]; //% B
       }
       else{
         valuePSOE = elem[0][0][1]; //% B
         valuePP = elem[0][1][1]; //% A
       }

       values_votesPP.push([key,valuePP]);
       values_votesPSOE.push([key,valuePSOE]);
     });


console.log(values_votesPP);
console.log(values_votesPSOE);
// end retrieving data from elections

// getting public spending data

var values_expense = [];
var province ="";
for ( var i = 1; i <= 19; i++) {

  province = getRepresentativeCity(i);

    var url_exp = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20json%20where%20url%20%3D%20%22http%3A%2F%2Ftransparenciadecuentaspublicas.es%2Fapi%2Fprovince_indicators%2F%3Fprovince%3D"+province+"%26indicator%3D28%26format%3Djson%22&format=json&callback=";
    $.ajax({
      url: url_exp,
      dataType: 'json',
      async: false,
      success: function(data) {
        var expense = data["query"]["results"]["json"]["results"][1]["value"]
        values_expense.push([i,parseInt(expense)]);
      }
    });
  }
  console.log(values_expense);

//end expense data

var testdata = [
{
  "key" : "PP" ,
  "color": "blue",
  "bar": true,
  "values" : values_votesPP
},
{
  "key" : "PSOE" ,
  "color": "red",
  "bar": true,
  "values" : values_votesPSOE
},
{
  "key" : "Spending" ,
  "bar":true,
  "color": "#383838",
  "values" : values_expense
}
].map(function(series) {
  series.values = series.values.map(function(d) { return {x: d[0], y: d[1] } });
  return series;
});
var chart;

nv.addGraph(function() {
  chart = nv.models.linePlusBarChart()
  .margin({top: 30, right: 60, bottom: 50, left: 70})
  .x(function(d,i) { return i })
  .color(d3.scale.category10().range());


  chart.y1Axis
  .tickFormat(d3.format(',f'));

  chart.y2Axis
  .tickFormat(function(d) { return '$' + d3.format(',.2f')(d) });

  chart.xAxis.tickFormat(function(d) {   return getZoneName(d) });


    //chart.y1Axis.tickFormat(function(d) { console.log(d); return "ROJO" });

    chart.y2Axis.tickFormat(function(d) {  return d });

    chart.bars.forceY([0]).padData(false);
    chart.lines.forceY([0]);
    chart.reduceXTicks = false;
    chart.staggerLabels= false;

    d3.select('#chart1 svg')
    .datum(testdata)
    .transition().duration(500).call(chart);

    nv.utils.windowResize(chart.update);

    chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

    return chart;
  });



