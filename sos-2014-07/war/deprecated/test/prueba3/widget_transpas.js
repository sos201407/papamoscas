/* Information comes from elpais.com and uses the following nomenclature (from INE):

01	Andalucía
02	Aragón
03	Asturias, Principado de
04	Balears, Illes
05	Canarias
06	Cantabria
07	Castilla y León
08	Castilla - La Mancha
09	Cataluña
10	Comunitat Valenciana
11	Extremadura
12	Galicia
13	Madrid, Comunidad de
14	Murcia, Región de
15	Navarra, Comunidad Foral de
16	País Vasco
17	Rioja, La
18	Ceuta
19	Melilla

*/




function getJsonFromZone(zone){

	var url = 'http://rsl00.epimg.net/elecciones/2014/europeas/'+zone+'/index.xml2';

	$.get(url, function(data) {
		var json = xml2json(data);
		console.log(json);
	});
}