/// MAP WIDGET

    function mapRefresh(){

     var watching_id = $("#input_id").val();
     console.log(apiPath + '/watchings/'+watching_id);
     $.get(apiPath + '/watchings/'+watching_id, function( data ) {
         $('#weather').show();
        console.log(data);

        // MAP Configuration
        var zoom = 15;
        var mapWidth = 400;
        var mapHeight = 400;
        var markerColor = "Red";
        var markerLabel = "Watching";
        var mapType = "satellite";
        // END MAP Configuration
        //var $selectedRows = $('#birdTable').jtable('selectedRows');
        var lat = data[0]["longitude"];
        var lng = data[0]["latitude"];
        

        var token = getURLParameter("access_token");
        console.log("Token: " + token);
        var urlLoc = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=false";
        var urlMap = "http://maps.google.com/maps/api/staticmap?" + "center=" + lat + "," + lng + "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&maptype=" + mapType + "&markers=color:" + markerColor + "|label:" + markerLabel + "|" + lat + "," + lng + "&sensor=" + "false" + "&token=" + token;
        var urlWea = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng + "&units=metric";
        console.log(urlLoc);
        console.log(urlMap);
        console.log(urlWea);


        var img = $("<img>");
        img.attr({
          src: urlMap
      });
        $("#map").html(img);
        var cosa = $("#map");
        console.log(cosa);


        console.log("Lat: " + lat + " Long: " + lng);

        $.getJSON(urlLoc, function(data) {
          var address = data["results"][0]["formatted_address"]
          console.log(address);
          $("#address").html(address);
      });

        $.getJSON(urlWea, function(data) {

          var weather = data["main"];
          var temp = weather["temp"];
          var hum = weather["humidity"];
          var press = weather["pressure"];
          var tempMin = weather["temp_min"];
          var tempMax = weather["temp_max"];

          console.log(temp + " " + hum + " " + tempMin + " " + tempMax);

          $("#temp").html("Temperature: " + temp + " ºC");
          $("#hum").html("Humidity: " + hum + " %");
          $("#press").html("Pressure: " + press + " mbar");
          //$("#tempMin").html(tempMin+" ºC");
          //$("#tempMax").html(tempMax+" ºC");
      });

    });

}

// end_ map widget main

// lightbox for map img

$(function lbox() {
    var content = $('#map').html();
    $('#map').click(function(e) {
        $('#map').lightbox_me({
            centered: true, 
            onLoad: function() { 
                $('#map').find('input:first').focus()
            },
            onClose: function() { 

              console.log(content);
              $('#map').html(content);
              $('#map').remove();
              $('#test3_div').append("<div id='map'</div>");
              $('#map').html(content);
              lbox();

          }
      });
        e.preventDefault();
    });

});


//end_ map widget
