myData = [];
function getData() {
	return $.ajax({
		url: "/api/v1/birds",
		type: 'GET',
	});
}



function handleData(data) {
	myData=data;
}

getData().done(handleData);




function printRadar(random){
random = typeof random !== 'undefined' ? random : false;  //default parameter

//Legend titles
var LegendOptions = ['Robledal','Pinar',];

//Data
var data = [];

var eggsR = 0;
var hatchesR = 0;
var wingsR = 0;
var legsR = 0;
var sizeR = 0;

var eggsP = 0;
var hatchesP = 0;
var wingsP = 0;
var legsP = 0;
var sizeP = 0;

if(random){
	myData[0]["place"]="robledal";
	myData[0]["eggs"]=1+Math.floor(Math.random()*100);
	myData[0]["eclosions"]=1+Math.floor(Math.random()*100);
	myData[0]["wingSize"]=1+Math.floor(Math.random()*100);
	myData[0]["legDiameter"]=1+Math.floor(Math.random()*100);

	myData[1]["place"]="pinar";
	myData[1]["eggs"]=1+Math.floor(Math.random()*100);
	myData[1]["eclosions"]=1+Math.floor(Math.random()*100);
	myData[1]["wingSize"]=1+Math.floor(Math.random()*100);
	myData[1]["legDiameter"]=1+Math.floor(Math.random()*100);
}

jQuery.each(myData,function(i){
	if(myData[i]["place"].toLowerCase() == "robledal"){
		eggsR += myData[i]["eggs"];
		hatchesR += myData[i]["eclosions"];
		wingsR += myData[i]["wingSize"];
		legsR += myData[i]["legDiameter"];
		sizeR ++;
	}else if(myData[i]["place"].toLowerCase() == "pinar"){
		eggsP += myData[i]["eggs"];
		hatchesP += myData[i]["eclosions"];
		wingsP += myData[i]["wingSize"];
		legsP += myData[i]["legDiameter"];
		sizeP ++;
	}else{
		console.log(myData[i]["place"].toLowerCase());
	}

});

// Mean

eggsR = eggsR/sizeR;
hatchesR = hatchesR/sizeR;
wingsR = wingsR/sizeR;
legsR = legsR/sizeR;

eggsP = eggsP/sizeP;
hatchesP = hatchesP/sizeP;
wingsP = wingsP/sizeP;
legsP = legsP/sizeP;



data.push([
	{axis:"Eggs",value:eggsR},
	{axis:"Wing size",value:wingsR},
	{axis:"Hatches",value:hatchesR},
	{axis:"Leg Diameter",value:legsR}
	]
	,
	[
	{axis:"Eggs",value:eggsP},
	{axis:"Wing size",value:hatchesP},
	{axis:"Hatches",value:wingsP},
	{axis:"Leg Diameter",value:legsP}
	]
	);

//Options for the Radar chart, other than default
var mycfg = {
	radius: 10,
	w: 500,
	h: 500,
	factor: 1,
	factorLegend: 1,
	levels: 10,
	maxValue: 1,
	radians: 2 * Math.PI,
	opacityArea: 0.5,
	ToRight: 0,
	TranslateX: 100,
	TranslateY: 100,
	ExtraWidthX: 200,
	ExtraWidthY: 200,
	color: d3.scale.category10()
}

//Call function to draw the Radar chart
//Will expect that data is in %'s
RadarChart.draw("#chart", data, mycfg);

////////////////////////////////////////////
/////////// Initiate legend ////////////////
////////////////////////////////////////////
var w = 0;
var h = 0;
var colorscale = colorScale = d3.scale.category10();
var svg = d3.select('#body')
.selectAll('svg')
.append('svg')
.attr("width", w)
.attr("height", h);


//Create the title for the legend
var text = svg.append("text")
.attr("class", "title")
.attr('transform', 'translate(90,0)') 
.attr("x", w - 70)
.attr("y", 10)
.attr("font-size", "12px")
.attr("fill", "#404040")
.text("% of birds");

//Initiate Legend	
var legend = svg.append("g")
.attr("class", "legend")
.attr("height", 100)
.attr("width", 200)
.attr('transform', 'translate(90,20)') 
;
	//Create colour squares
	legend.selectAll('rect')
	.data(LegendOptions)
	.enter()
	.append("rect")
	.attr("x", w - 65)
	.attr("y", function(d, i){ return i * 20;})
	.attr("width", 10)
	.attr("height", 10)
	.style("fill", function(d, i){ return colorscale(i);})
	;
	//Create text next to squares
	legend.selectAll('text')
	.data(LegendOptions)
	.enter()
	.append("text")
	.attr("x", w - 52)
	.attr("y", function(d, i){ return i * 20 + 9;})
	.attr("font-size", "11px")
	.attr("fill", "#737373")
	.text(function(d) { return d; })
	;	



}


