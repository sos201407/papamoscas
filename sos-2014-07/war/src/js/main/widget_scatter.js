google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);

$myData = [];
function getData() {
	return $.ajax({
		url: "/api/v1/locations",
		type: 'GET',
	});
}

function deleteData() {
	return $.ajax({
		url: "/api/v1/locations/",
		type: 'DELETE'
	});
}


function setData() {
	var id = 1 + Math.floor(Math.random() * 999);
	var nvdi = 1 + Math.floor(Math.random() * 100);
	var temp = 1 + Math.floor(Math.random() * 45);
	var rp = 1 + Math.floor(Math.random() * 2);
	var place = (rp==1) ? "Robledal" : "Pinar";
	var json = '{"date" : "2009-10-09",	"id" : "'+id+'","nvdi" : '+nvdi+',"place" : "'+place+'","temp" : '+temp+'}';
	console.log(json);
	return $.ajax({
		url: "/api/v1/locations",
		type: 'POST',
		dataType: 'json',
		data: json
	});
}

function handleData(data) {
	//console.log(data);
	$myData=data;
}

getData().done(handleData);

function drawChart() {

	var dataTable = new google.visualization.DataTable();
	dataTable.addColumn("number","x")
	dataTable.addColumn("number","Robledal")
	dataTable.addColumn("number","Pinar")
	$.each($myData, function(i, val) {
		if(val["place"].toLowerCase() == 'robledal'){
			dataTable.addRow([val["nvdi"],val["temp"],null]);
		}

		if(val["place"].toLowerCase() == 'pinar'){
			dataTable.addRow([val["nvdi"],null,val["temp"]]);
		}
	});

	var options = {
		height: 300,
		width: 400,
		title: 'Temperature vs. NVDI',
		hAxis: {title: 'Temperature'},
		vAxis: {title: 'NVDI'},
		selectionMode: 'multiple',
		tooltip: { trigger: 'selection' },
		aggregationTarget: 'category',
		animation:{
			duration: 2000,
			easing: 'inAndOut',
		},
		

		annotations: {
			boxStyle: {
				stroke: '#888',           
				strokeWidth: 1,          
				rx: 10,                   
				ry: 10,                   
				gradient: {               
					color1: '#fbf6a7',      
					color2: '#33b679',      
					x1: '0%', y1: '0%',     
					x2: '100%', y2: '100%', 
					useObjectBoundingBoxUnits: true 
				}
			}  
		},
		crosshair: {
			trigger: 'both' 
		},
		curveType: 'function',      
		trendlines: {
			0: {
				labelInLegend: 'Robledal trend',
				visibleInLegend: false,
				lineWidth: 10,
				opacity: 0.2,
				type: 'exponential',
			},
			1: {
				labelInLegend: 'Pinar trend',
				visibleInLegend: false,
				lineWidth: 10,
				opacity: 0.2,
				type: 'exponential'

			}
		},
		explorer: { actions: ['dragToZoom', 'rightClickToReset'] }
	};

	var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

	chart.draw(dataTable, options);






}

$(function() {

	$("#add").click(function() {
		alertify.set({ delay: 1000 });
		alertify.success("Loading");

		getData().done(handleData);
		drawChart();
		setData();

		setTimeout("getData().done(handleData)",1000);
		setTimeout("drawChart()",100);
	});


	$("#delete").click(function() {
		alertify.set({ delay: 3000 });
		alertify.error("Loading");

		getData().done(handleData);
		drawChart();

		deleteData();

		setTimeout("getData().done(handleData)",1000);
		setTimeout("drawChart()",100);

	});



	$("#refresh").click(function() {

		getData().done(handleData);
		setTimeout("drawChart()",100);

	});
});