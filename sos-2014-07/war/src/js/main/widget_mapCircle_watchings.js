var MapCircleOverlay = function(center, radius, strokeWeight, strokeColor, strokeOpacity, fillColor, fillOpacity) {
	this.center = center;
	this.radius = radius;
	this.strokeWeight = strokeWeight;
	this.strokeColor = strokeColor;
	this.strokeOpacity = strokeOpacity;
	this.fillColor = fillColor;
	this.fillOpacity = fillOpacity;
 
	this.circlePolygon = null;
 
	// 50 lines look like a pretty good circle
	this.numPoints = 50;
 
	this.d2r = Math.PI / 180;
 
	this.bound = null;
 
	this.setCenter = function( latLng ) {
		this.center = latLng;
		this.draw();
	};
 
	this.setRadius = function( radius ) {
		this.radius = radius;
		this.draw();
	};
};
 
/* base class overloads follow this comment */
MapCircleOverlay.prototype = new google.maps.OverlayView;
 
// Calculate all the points and draw them
// Base method must be implemented like this
MapCircleOverlay.prototype.draw = function() {	
	if ( ! isFinite( this.radius ) || ! isFinite( this.center.lat() ) || ! isFinite( this.center.lng() ) ) {
		if ( console != undefined ) 
			console.error('Radius has to be a number !');
		return;
	}
 
	circleLatLngs = new Array();
 
        // Remove the "* 0.621371192" to use miles instead of kilometers
	var circleLat = this.radius * 0.621371192 * 0.014483;  // Convert statute into miles and miles into degrees latitude
	var circleLng = circleLat / Math.cos( this.center.lat() * this.d2r);
 
	// 2PI = 360 degrees, +1 so that the end points meet
	for (var i = 0; i < this.numPoints+1; i++) { 
		var theta = Math.PI * (i / (this.numPoints / 2)); 
		var vertexLat =  this.center.lat() + (circleLat * Math.sin(theta)); 
		var vertexLng =  this.center.lng() + (circleLng * Math.cos(theta));
		var vertextLatLng = new google.maps.LatLng(vertexLat, vertexLng);
		circleLatLngs.push( vertextLatLng );
	}
 
	// Before drawing the new polygon, we have to remove the old one
	this.clear();
 
	this.circlePolygon = new google.maps.Polygon({
	  paths: circleLatLngs,
	  strokeColor: this.strokeColor,
	  strokeOpacity: this.strokeOpacity,
	  strokeWeight: this.strokeWeight,
	  fillColor: this.fillColor,
	  fillOpacity: this.fillOpacity
	});
 
	this.circlePolygon.setMap( this.map );
};
 
MapCircleOverlay.prototype.clear = function() {
	if ( this.circlePolygon != null ) {
		this.circlePolygon.setMap( null );
		this.circlePolygon = null;
	}
};
 
MapCircleOverlay.prototype.onRemove = function() {
	this.clear();
};