   
//grid initialization

var gridster;

$(function(){

	gridster = $(".gridster > ul").gridster({
		widget_margins: [10, 50],
		widget_base_dimensions: [400, 300],
		draggable: {
			handle: 'header'
		},
		min_rows:1,
		min_cols: 2
	}).data('gridster');

	var widgets = [
	['<li><header>Temperature vs NVDI</header><div id="chart_div"</div></li>', 1, 1],

	['<li><header>Sightings close to you</header><div id="oauth"></div> <div id="oauth_div" style="min-width: 400px; height: 300px; margin: 0 auto"></div> </li>', 1, 1],

	['<li><header>Spending vs Votes</header><div id="nvd3_div" style="min-width: 400px; height: 300px; margin: 0 auto"></div> <a href="http://transparenciadecuentaspublicas.es/"><img src="http://transparenciadecuentaspublicas.es/static/tdcp/images/logo_banner_500x150.jpg" style=" margin-top: -2em; width: 11em; "></a></li>', 1, 1],

	['<li><header>Population Characteristics</header><div id="radar_div" style="min-width: 400px; height: 300px; margin: 0 auto"></div></li>', 1, 1],

	['<li><header>Watching search</header><div id="map_div" style="min-width: 400px; height: 300px; margin: 0 auto"></div></li>', 1, 2],

	['<li><header>Bird Search</header><div id="image_div" style="min-width: 400px; height: 300px; margin: 0 auto"></div></li>', 1, 1]


	];

	$.each(widgets, function(i, widget){
		gridster.add_widget.apply(gridster, widget)
	});

});

   function getURLParameter(sParam){
   	var sPageURL = window.location.hash.substring(1);
   	var sURLVariables = sPageURL.split('&');
   	for (var i = 0; i < sURLVariables.length; i++) {
   		var sParameterName = sURLVariables[i].split('=');
   		if (sParameterName[0] == sParam) 
   			return sParameterName[1];
   	}
   }

// (some) widgets initilization

$(function () {
	printRadar();

	$('#map_div').html('<input type="text" id="input_id" class="form-control" placeholder="Watching ID" onkeydown="if (event.keyCode == 13) $(\'#input_refresh\').click()"> <button type="button" id="input_refresh" class="btn btn-xs btn-default"onClick="mapRefresh()">Refresh</button><div id="address"></div> <div id="map"></div> <br>  <div id="weather"> <p>Current weather:</p> <div> <p id="temp">Temperature</p> <p id="hum" >Humidity</p> <p id="press">Pressure</p> </div> </div> ');

	$('#image_div').html('<input type="text" id="inputBird_id" class="form-control" placeholder="Bird ID" onkeydown="if (event.keyCode == 13) $(\'#inputBird_refresh\').click()"> <button type="button" id="inputBird_refresh" class="btn btn-xs btn-default"onClick="birdRefresh()">Refresh</button> <div id="photoInfo"></div> <div id="photo"></div> <div id="listenBird"></div> <div id="sendBird"></div>');

	$('#nvd3_div').html('<div id="chart1" class="with-3d-shadow with-transitions"> <svg> </svg> </div>');



	$('#weather').hide();
	$("#photoInfo").hide();
	$("#photo").hide();
	$("#listenBird").hide();
	$("#sendBird").hide();
	
	var scope = 'https://www.googleapis.com/auth/plus.login';
	var redirect_uri = 'http://sos-2014-07.appspot.com/dashboard/dashBirds.html';
	var client_id = '690561286617-io3bqcij7qs1pts9lgoqh2cteiehvpkm.apps.googleusercontent.com';

	var oAuth_url = 'https://accounts.google.com/o/oauth2/auth?scope='+scope+'&response_type=token&redirect_uri='+redirect_uri+'&client_id='+client_id;


	var button = '<form action='+oAuth_url+' method="post"> <button id="oAuth_btn" class="btn btn-success">Login!</button> </form>'
	$('#oauth').html(button);
    // we alse should check the time to live of token, nevertheless, we use static content once loaded. Doesn't matter.

    $('#oauth').show();

	// get parameter for oauth token



    //oAuth handling

    var scope = 'https://www.googleapis.com/auth/plus.login';
    var redirect_uri = 'http://sos-2014-07.appspot.com/dashboard/dashBirds.html';
    var client_id = '690561286617-io3bqcij7qs1pts9lgoqh2cteiehvpkm.apps.googleusercontent.com';

    var oAuth_url = 'https://accounts.google.com/o/oauth2/auth?scope='+scope+'&response_type=token&redirect_uri='+redirect_uri+'&client_id='+client_id;


    var button = '<form action='+oAuth_url+' method="post"> <button id="oAuth_btn" class="btn btn-success">Login!</button> </form>'
    $('#oauth').html(button);
    // we alse should check the time to live of token, nevertheless, we use static content once loaded. Doesn't matter.

    var token = getURLParameter("access_token");
    console.log("Token: " + token);

    if(typeof token === "undefined"){ //token does not exist
    	$('#oauth').show();
    }else{
    	$('#oauth').hide();

    	console.log(oAuth_url);

    	var urlProfile = "https://www.googleapis.com/plus/v1/people/me";


		$.ajax({ //user to profile
			type:"GET",
			beforeSend: function (request)
			{
				request.setRequestHeader("Authorization", "Bearer "+token);
			},
			url: urlProfile,
			success: function(data) {
				var name = data["displayName"];
				var place = data["placesLived"][0]["value"];
				var urlImage = data["image"]["url"];
				console.log(data);

				var urlLoc = "http://maps.googleapis.com/maps/api/geocode/json?address="+place+"&sensor=false&token="+token;
				console.log(urlLoc);
		        ////
		        $.getJSON(urlLoc, function(data) { //place to latlng
		        	var lat = data["results"][0]["geometry"]["location"]["lat"];
		        	var lng = data["results"][0]["geometry"]["location"]["lng"];
		            ////$.getJSON(urlLoc, function(data) { //place to latlng
		            	var lat = data["results"][0]["geometry"]["location"]["lat"];
		            	var lng = data["results"][0]["geometry"]["location"]["lng"];
		                ///
		                var urlBirds = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20xml%20where%20url%20%3D%20%22http%3A%2F%2Febird.org%2Fws1.1%2Fdata%2Fobs%2Fgeo%2Frecent%3Flng%3D"+lng+"%26lat%3D"+lat+"%22&format=json&diagnostics=true&callback=";
		                console.log(urlBirds);
		            $.getJSON(urlBirds, function(data) { //latlng to nearby birds
		            	var sightings = data["query"]["results"]["response"]["result"]["sighting"];
		            	console.log(sightings);

		                //here we have sighting near from user locations got by oAuth

		                $("#oauth_div").append('<div id=\"nearbyBird\"></div>');
		                $("#nearbyBird").append("<ul>");
		                $.each( sightings, function( k,sight ) {
		                	var sci = sight["sci-name"];
		                	var loc = sight["loc-name"];
		                	$("#nearbyBird").append("<li>A <b>"+sci+"</b> has been seen at "+loc+"</li>");
		                });
		                $("#nearbyBird").append("</ul>");


            });

        });
}
});

}

});    

