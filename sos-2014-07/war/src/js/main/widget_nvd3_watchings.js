
$myDataNVD3 = [];

function getDataNVD3() {
    return $.ajax({
      url: "/api/v1/locations",
      type: 'GET',
    });
}

function handleDataNVD3(data) {
    console.log(data);
    $myDataNVD3=data;
}

getDataNVD3().done(handleDataNVD3);


window.onload=function(){

	var data= function() {

		var jsonArr = [];
		var tempRobledal = [];
		var tempPinar = [];
		var nvdiRobledal = [];
		var nvdiPinar = [];
		
		$.each($myDataNVD3, function(i, val) {
    if(val["place"] == "Robledal"){
      tempRobledal.push({"x":val["date"],"y":val["temp"]})
      nvdiRobledal.push({"x":val["date"],"y":val["nvdi"]})
    }
    if(val["place"] == "Pinar"){
      tempPinar.push({"x":val["date"],"y":val["temp"]})
      nvdiPinar.push({"x":val["date"],"y":val["nvdi"]})
    }
  }); 

		jsonArr.push({
			key: "Robledal Temperature",
			values: tempRobledal
		});

		jsonArr.push({
			key: "Pinar Temperature ",
			values: tempPinar
		});

		jsonArr.push({
			key: "Robledal NVDI",
			values: nvdiRobledal
		});

		jsonArr.push({
			key: "Pinar NVDI ",
			values: nvdiPinar
		});

		return jsonArr;
	}

	nv.addGraph(function() {
		var chart = nv.models.multiBarChart();
		chart.tooltips(true);
		chart.yAxis.tickFormat(d3.format(',.1f'));
		chart.xAxis
		chart.multibar.stacked(false);
		chart.showControls(false);

		d3.select('#chart svg').datum(data()).transition().duration(500).call(chart);
		nv.utils.windowResize(chart.update);
		return chart;
	});

}//]]> 