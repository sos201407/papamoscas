
$dataCityName = [];
$dataCity = [];


var provinceID = "";
var cityName;
var provinceName;
var autonomic;


// #################  Get from Lat Long City and Village Name ##########
function getCityFromLatLong(myLat,myLong) {
	console.log("Inside getCity "+myLat,myLong);
    return $.ajax({
        url: "http://maps.googleapis.com/maps/api/geocode/json?latlng="+myLat+","+myLong+"+&sensor=true_or_false",
        type: 'GET',
        async:false
    });
}
  
function handleCityName(data) {
      $dataCityName=data;
}




function processCityLatLong(latGlobal,longGlobal){
	console.log("Inside getCity " +latGlobal,longGlobal);
	getCityFromLatLong(latGlobal,longGlobal).done(handleCityName);
	cityName = $dataCityName.results[2].address_components[0].short_name;
	provinceName = $dataCityName.results[2].address_components[1].long_name;
	autonomic = $dataCityName.results[2].address_components[2].long_name; 
}



// #################  Get from using City Name, the  ##########
function getCityData(city) {
	city = city.replace(/\s/g, "%20");
    return $.ajax({
        url: "http://query.yahooapis.com/v1/public/yql",
        type: 'GET',
        data: { q: "select * from json where url='http://transparenciadecuentaspublicas.es/api/aapps/?search="+city+"&ordering=-population&format=json&page_size=1'", 
        format: "json"},
        async:false
    });
}
  
function handleCityData(data) {
      $dataCity=data;
}

function processCity (cityName){
	getCityData(cityName).done(handleCityData);
}



// #################  Indicadores   ##########

function getProvinceData(provinceID) {
	
    return $.ajax({
        url: "http://query.yahooapis.com/v1/public/yql",
        type: 'GET',
        data: { q: "select * from json where url='http://transparenciadecuentaspublicas.es/api/province_indicators/?province="+provinceID+"&indicator=6&format=json&page_size=1'", 
        format: "json"},
        async:false
    });
}  
function handleProvinceData(data) {
      $dataProvince=data;
}
function processProvinceData (provinceID){
	getProvinceData(provinceID).done(handleProvinceData);
}




function cityDetails(latGlobal,longGlobal) {
	console.log("Inside getCity " +latGlobal,longGlobal);

	processCityLatLong(latGlobal,longGlobal);
	var str ="<br />"
    str +="<button type='button' id='logout' class='btn btn-lg btn-default'onClick='Logout()'>Logout </button>";
	document.getElementById("status").innerHTML=str;

	processCity(cityName);


	provinceID = $dataCity.query.results.json.results.province.split('/')[5]; 
	console.log (provinceID);
	processProvinceData(provinceID);

	var infoCity = '<div id="cityInfo"><h2> City Info  </h2><br/ ><hr />'+
		'<span class="info-content">'+
		'<b>City</b>  : '+cityName+
		'<br /><b>Province </b> : '+provinceName+ 
		'<br /><b>Autonomic</b> : '+autonomic+ 
		'<br /><b>Population</b> : '+$dataCity.query.results.json.results.population+ 
		'<br /><b>Gastos en Servicios </b> : '+$dataProvince.query.results.json.results.value+ ' '+$dataProvince.query.results.json.results.units + 
		'</span>'+
		'</div>';
    document.getElementById("infoCity").innerHTML=infoCity;
}

