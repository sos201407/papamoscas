var geocoder;
var map;
var center = "";
var stack = [];
var myMarkers = [];
var radiusGlobal = 0;

var latGlobal=0;
var longGlobal=0;


$dataMap = [];

function getDataMap() {
    return $.ajax({
        url: "http://sos-2014-07.appspot.com/api/v1/watchings",
        type: 'GET',
        async:false
    });
}
  
function handleDataMap(data) {
      $dataMap=data;
}

getDataMap().done(handleDataMap);


function getMarkers (map){
    var markers = [];
    var infowindow = new google.maps.InfoWindow({});

	$.each($dataMap, function(i, val) {

	    var gui = val["GUI"];
	    var species = val["species"];
	    var commonName = val["commonName"];
	    var watchSiteName = val["watchSiteName"];
	    var latitude = val["latitude"];
	    var longitude = val["longitude"];
	    var date = val["date"];
	    var point = new google.maps.LatLng(latitude,longitude);
	    // var html = "<b>" + gui + "</b> <br/>" + species + "<br/>" + commonName + "<br/>" + watchSiteName;

	    var marker = new google.maps.Marker({
	      map: map,
	      position: point
	      ,animation: google.maps.Animation.DROP
    	});

	    google.maps.event.addListener(marker, 'click', (function (marker, i) {
    		return function () {
       			var infoMarker = '<div class="info-win">'+
        			'<span class="info-content">'+
	        		'<h2 > Bird  : '+commonName + ' </h2><hr />'+
	        		'Species : '+species+ 
	        		'<br />Site of Watching : '+watchSiteName+ 
	        		'<br />Latitude : '+latitude+ 
	        		'<br />Longitude : '+longitude+ 
	        		'<br />Date : '+date+ 
	        		'</span>'+
	        		'</div>';

	        	infowindow.setContent(infoMarker);
        		infowindow.open(map, marker);
        		myMarkers.push(infoMarker);
        	}
    	})(marker, i));    		
    	stack.push(marker);

	});	
	return stack;
}



function initialize() {	
	geocoder = new google.maps.Geocoder();
	var center_var = new google.maps.LatLng(40.4378271, -3.6795367)
	map = new google.maps.Map(document.getElementById('map-canvas'), {
		zoom: 5,
       	center: center_var,
		mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    mcOptions = {styles: [{
		height: 53,
		url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
		width: 53
		},
		{
		height: 56,
		url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m2.png",
		width: 56
		},
		{
		height: 66,
		url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m3.png",
		width: 66
		},
		{
		height: 78,
		url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m4.png",
		width: 78
		}]
	}

	var stack = getMarkers(map);    
	var mc = new MarkerClusterer(map, stack,mcOptions);	
}



function codeLocation(response) {

  geocoder.geocode( { 'address': response.location.name}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {

    	var str ="<br />"
    	str +="<button type='button' id='logout' class='btn btn-lg btn-default'onClick='Logout()'>Logout </button>";
		str +="<button type='button' id='getClosest' class='btn btn-lg btn-default'onClick='getClosest()'> getClosest</button>";
    	document.getElementById("status").innerHTML=str;
    	
		map.setCenter(results[0].geometry.location);
		center = results[0].geometry.location;

		var marker = new google.maps.Marker({
			map: map,
			position: results[0].geometry.location
		});
		marker.info = new google.maps.InfoWindow({
			content: '<div id="marker">'+
						'<b>Name</b> : '+response.name+'<br> ' +
						'<b>location</b> : '+response.location.name+'<br> '+
						'<img src="https://graph.facebook.com/'+ response.id+'/picture " />'+
						'</div>'
		});

		google.maps.event.addListener(marker, 'click', function() {
			marker.info.open(map, marker);
		});


    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
 
}

function getCircle(){
	var circle = new MapCircleOverlay(	center,	radiusGlobal , "#FF0000", 0.8, 3, "#FF0000", 0.35 );
	circle.setMap( map );
	map.setZoom(8);
	map.setCenter(center);
}

			 
function rad(x) {return x*Math.PI/180;}

function getClosest() {
	

    
    var lat = center.k;
    var lng = center.A;
    var R = 6384; // radius of earth in km
    var distances = [];
    var closest = -1;
    
    $.each($dataMap, function(j,val) {
		var mlat = val["latitude"];
	    var mlng = val["longitude"];

		//console.log(j,val);
        //console.log("Lat,Long " +mlat,mlng);

        var dLat  = rad(mlat - lat);
        var dLong = rad(mlng - lng);
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;

        distances[j] = d;
        if ( closest == -1 || d < distances[closest] ) {
            closest = j;
        }
	});	

    console.log($dataMap[closest],distances[closest]);
    radiusGlobal = distances[closest];
    getCircle();

    latGlobal= $dataMap[closest]["latitude"];
    longGlobal = $dataMap[closest]["longitude"];

    var str ="<br />"
    str +="<button type='button' id='logout' class='btn btn-lg btn-default'onClick='Logout()'>Logout </button>";
    str +="<button type='button' id='cityDetails' class='btn btn-lg btn-default'onClick='cityDetails("+ latGlobal+","+ longGlobal+")'>getCityDetails </button>";
	document.getElementById("status").innerHTML=str;

    var infoBird = '<div class="birdClosest"><h2> Closest Bird</h2><br/ ><hr />'+
        			'<span class="info-content">'+
	        		'<b>Bird</b>  : '+$dataMap[closest]["commonName"]+
	        		'<br /><b>Species</b> : '+$dataMap[closest]["species"]+ 
	        		'<br /><b>Site of Watching</b> : '+$dataMap[closest]["watchSiteName"]+ 
	        		'<br /><b>Date</b> : '+$dataMap[closest]["date"]+ 
	        		'<br /><b>Distance</b> : '+ Math.floor(radiusGlobal * 100)/100+
	        		'</span>'+
	        		'</div>';


    document.getElementById("closest").innerHTML+=infoBird;
}



google.maps.event.addDomListener(window, 'load', initialize);



var fb_estado = 0;
window.fbAsyncInit = function() {

	FB.init({
		appId      : '1404579703162535', // Set YOUR APP ID
		channelUrl : 'channel.html', // Channel File
		status     : true, // check login status
		//cookie     : true, // enable cookies to allow the server to access the session
		//xfbml      : true,  // parse XFBML
		version    : 'v2.0'
	});
				 
	FB.Event.subscribe('auth.authResponseChange', function(response){
		if (response.status === 'connected') {
			//SUCCESS
			
			if(fb_estado<1){
				fb_estado +=1;
				Login();
			}
			//document.getElementById("message").innerHTML +=  "<br>Connected to Facebook";
		
		} 
		else if (response.status === 'not_authorized'){

			document.getElementById("message").innerHTML +=  "<br> ERROR : Failed to Connect";
				 
				        
		} 
		else {
			//UNKNOWN ERROR		
			document.getElementById("message").innerHTML +=  "<br>ERROR : Logged Out";
		}
	}); 
};

function getPic(){
		FB.api('/me/picture?type=normal', function(response) {
			var str="<b>Pic</b> : <img src='"+response.data.url+"'/>";
			document.getElementById("marker").innerHTML+=str; 
		});
	}
function Login(){

	FB.login(function(response) {
		if (response.authResponse) {
			getUserInfo();
		} 
		else {
			console.log('User cancelled login or did not fully authorize.');
			}
		},{scope: 'user_photos,user_location,public_profile,email'});
				 
	}
	
	function geoCode(){

		FB.api('/v2.0/me', function(response) {			
			var location = response.location.name;
			codeLocation(response);
		});

	}			 
	function getUserInfo() {
		FB.api('/v2.0/me', function(response) {
			console.log(response);
			
			//var str="<b>Name</b> : "+response.name+"<br>";
			// str +="<b>location:</b> "+response.location.name+"<br>";
			// str +="<b>id: </b>"+response.id+"<br>";
			// str +="<b>Email:</b> "+response.email+"<br>";
			// str +="<input type='button' value='Get Photo' onclick='getPhoto();'/>";
			var str ="<br />"
			str +="<button type='button' id='logout' class='btn btn-lg btn-default'onClick='Logout()'>Logout </button>";
			str +="<button type='button' id='geoCode' class='btn btn-lg btn-default'onClick='geoCode()'> GeoCode</button>";
			document.getElementById("status").innerHTML=str;

		});
	}

	function getProfilePic(){
		FB.api('/me/picture?type=normal', function(response) {
			var str="<b>Pic</b> : <img src='"+response.data.url+"'/>";
			// document.getElementById("marker").innerHTML+=str; 

		});
	}

	function Logout(){
		FB.logout(function(){document.location.reload();});
	}
				 
	// Load the SDK asynchronously
	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));
				 

	
