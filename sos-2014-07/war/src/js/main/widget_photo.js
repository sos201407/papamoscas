var birdData;

function birdRefresh(){

    // Flickr configuration

    var bird_id = $("#inputBird_id").val();
    var api_key="aa1c1d1afacebcdb31bd7901b91ccf8d";
    var text = "";
    var photo_id = "";
    var photo = ""

    $.get(apiPath + '/birds/'+bird_id, function( data ) {
        text =  data["specie"];
        birdData = data;


        var urlPhotoQuery = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="+api_key+"&text="+text+"&format=json&nojsoncallback=1";

// retrieving photo id by selecting a random photo from query
$.get(urlPhotoQuery, function( data ) {
    //console.log(urlPhoto);
    var photo_arr = data["photos"]["photo"];
    //console.log(photo_arr);
    var randIndex = Math.floor(Math.random() * (photo_arr.length - 0 + 1)) + 0;
    var photo_id = photo_arr[randIndex]["id"];
    //console.log(photo_arr[randIndex]);
    //console.log(photo_id);



    var urlPhoto =  "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key="+api_key+"&photo_id="+photo_id+"&format=json&nojsoncallback=1";
    //console.log(urlPhoto);

    $.get(urlPhoto, function( data ) {
        //console.log(data);
        photo = data["sizes"]["size"][4]["source"];
        
        var img = $("<img>");
        img.attr({
          src: photo,
          title:text
      });

        


    //// bird sounds
    console.log(text);
    var textEscaped = escape(text);
    console.log(textEscaped);
    var urlBirdSounds = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20json%20where%20url%3D%22http%3A%2F%2Fwww.xeno-canto.org%2Fapi%2F2%2Frecordings%3Fquery%3D'+escape(textEscaped)+'%26q%3AA%22&format=json&callback='
    console.log(urlBirdSounds);


    $.ajax({
        type: "GET",
        url: urlBirdSounds,
    })
    .success(function(data) {
        console.log(data);
        var soundList = data["query"]["results"]["json"]["recordings"];
        var numSounds = data["query"]["results"]["json"]["numRecordings"];
        console.log(soundList);
        console.log(numSounds);
        var randSoundIndex = Math.floor(Math.random() * (parseInt(numSounds) - 0 + 1)) + 0;
        console.log(randSoundIndex);
        var soundFile = soundList[randSoundIndex]["file"];
        console.log(soundFile);

        var birdInfo = "<p>"+text+"</p>";
        var birdButton = '<button type="button" id="pushBirdButton" title="IMPORTANT: Due to Pushbullet API restrictions, we only are able to sent push to certain registered devices. If you like yours was here, please, talk to us." class="btn btn-success" onClick="pushBird()">Push to phone!</button>';
        var sound = '<audio controls>  <source src="'+soundFile+'" type="audio/ogg"> </audio>';

        $("#photoInfo").html(birdInfo);
        $("#photo").html(img);
        $("#listenBird").html(sound);
        $("#sendBird").html(birdButton);


        $("#photoInfo").show();
        $("#photo").show();
        $("#listenBird").show();
        $("#sendBird").show();

    });

});

});
});



}


function pushBird(){
    console.log(birdData);

    // PushBullet configuration
    var username = "v1UBE1rES6Xy7XytKLm8YljAS3B0vz5fzmujCPRkHUiYK";
    var password = "";  
    var device_iden = "ujCPRkHUiYKdjzWIEVDzOK"
    // end

    //encoding user/pass for basic http auth
    function make_base_auth(user, password) {
      var tok = user + ':' + password;
      var hash = btoa(tok);
      return "Basic " + hash;
  }
  $.ajax
  ({
    type: "POST",
    url: "https://v1UBE1rES6Xy7XytKLm8YljAS3B0vz5fzmujCPRkHUiYK@api.pushbullet.com/v2/pushes",
    dataType: 'json',
    async: false,
    contentType: "application/json",
    data: JSON.stringify( {
        type:"list",
        device_iden:device_iden,
        title:"Bird",
        items:["Id: "+birdData["id"],"Specie: "+birdData["specie"], "Place: "+birdData["place"], "Leg Diameter: "+birdData["legDiameter"], "Wing Size: "+birdData["wingSize"], "Eggs: "+birdData["eggs"], "Hatches: "+birdData["eclosions"]]

    } ),
    beforeSend: function (xhr){ 
        xhr.setRequestHeader('Authorization', make_base_auth(username, password)); 
    },
    success: function (){
        console.log('Push sent successfully, please, check your phone :)'); 
    }
});
}
