var interfacesos_1_1us_1_1es_1_1_location =
[
    [ "getDate", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#a18891dd75519b227d392d71a3a15c741", null ],
    [ "getID", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#ac3592c38a62f7bb2f40f4c313858b688", null ],
    [ "getNVDI", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#ae0e6d19bded01598af1ba7924a884141", null ],
    [ "getPlace", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#a0fabe6225eaea7b0317a66b682b6a6de", null ],
    [ "getTemp", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#a9ac77f28aec9b15602b9cad5374def45", null ],
    [ "setDate", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#a2aa177e988855b562319a29e23b528e7", null ],
    [ "setID", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#a0405e6bc9f339feb39094b5b23f1c698", null ],
    [ "setNVDI", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#a5ac1de356275ba9f4e9ac0c7d65c4024", null ],
    [ "setPlace", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#ab731abac0c4f9463a038f592e41d4554", null ],
    [ "setTemp", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html#a8012e9defd3d26c8052b05f27834ec4a", null ]
];