var interfacesos_1_1us_1_1es_1_1_birdwatching =
[
    [ "getCommonName", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#a61e727b00699fd468da77645a902156a", null ],
    [ "getDate", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#ac10450ed0f6696c5f33bbf86e93c581b", null ],
    [ "getGUI", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#a4c2dfc613e2c9834416c64c2aac26a2e", null ],
    [ "getLatitude", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#a2b74f1a743a795a3872f6ca3f57c1534", null ],
    [ "getLongitude", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#af9d8a836bc624b6e72ad66215f758c12", null ],
    [ "getSpecies", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#ac0c923602499fdb841755a28f4c12fc6", null ],
    [ "getwatchSiteName", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#a2109f3bd08097db14430a07b57c10e8a", null ],
    [ "setCommonName", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#abdc630f9a83b1977b4fd64f0f9d80388", null ],
    [ "setDate", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#a5d1ea3fd7b31d661b1d1086c89efcc8c", null ],
    [ "setGUI", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#ac62f5c7f8755b0ade6303e5c287f406a", null ],
    [ "setLatitude", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#a373065dd97d30e1f5fad82adcf12cfde", null ],
    [ "setLongitude", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#a0a3627cb3a24e3ecdd7f70e0202723b9", null ],
    [ "setSpecies", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#ac05471d88cd6ce064d184633828bd571", null ],
    [ "setwatchSiteName", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html#a8d6f9abc371f0a21d971594da05fcacd", null ]
];