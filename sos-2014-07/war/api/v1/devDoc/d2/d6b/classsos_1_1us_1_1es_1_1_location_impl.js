var classsos_1_1us_1_1es_1_1_location_impl =
[
    [ "LocationImpl", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#a49e9c97f236ce9b2782acc91358262d7", null ],
    [ "LocationImpl", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#a9a9c3f71ec7ad603bef94b7280f0c212", null ],
    [ "getDate", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#aa014d3a195c1a10460b693cd0c200680", null ],
    [ "getID", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#a94fd1499b2a671d9f63a62f0e36cfad2", null ],
    [ "getNVDI", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#a06958ce5e77f77ecf4edb976d2d04517", null ],
    [ "getPlace", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#ac60eb70fb5b346e21b8f6e1089e396f6", null ],
    [ "getTemp", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#a62120abdedf388bbfd91c75f323d9787", null ],
    [ "setDate", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#a9dc6df8503a721e3146cb4c3437eac4b", null ],
    [ "setID", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#a156c7f5614af7a320b3dfbf3b19ab473", null ],
    [ "setNVDI", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#ac7c2fcb438c2668a9b0f54deeb339074", null ],
    [ "setPlace", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#a67c3aecda571887d946f9257efe3a516", null ],
    [ "setTemp", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html#a68fa3c00e3f0179625ee2db252c7a353", null ]
];