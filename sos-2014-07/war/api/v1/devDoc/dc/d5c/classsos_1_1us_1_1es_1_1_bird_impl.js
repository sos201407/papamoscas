var classsos_1_1us_1_1es_1_1_bird_impl =
[
    [ "BirdImpl", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#af61a932e84f63e0cc84e0f1e2a61313b", null ],
    [ "BirdImpl", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a2b940bca68eaf00b14fc3c6ab9f89f85", null ],
    [ "getEclosions", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a0bf5b4f70ad97f75fdf711681b69c0e9", null ],
    [ "getEggs", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#acbf099562063a15d5e19f7d53bea7850", null ],
    [ "getId", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a3193edb18fa7639f763c9a889f0cc190", null ],
    [ "getLegDiameter", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a0a5b558aff1420ddee7aa94ffcb420ba", null ],
    [ "getPlace", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#aec8aa245ddbbf3c82407eaa5002f17a1", null ],
    [ "getSpecie", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#ae528cea76c889d305476da1dcd88d003", null ],
    [ "getWingSize", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a4b199513ef2bdff27eb9ce2ebac48722", null ],
    [ "setEclosions", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a6093c3dc407687b168481435a378b4c8", null ],
    [ "setEggs", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#aae750b57fde9fcc75802d21cfa402121", null ],
    [ "setId", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a06398f70071c9a647c7f53d5a723b0f4", null ],
    [ "setLegDiameter", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a992d2222f73b9f5e5fa78b7c762bb0fb", null ],
    [ "setPlace", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#aa05d5b5c72922f2a7896ec341aad6f69", null ],
    [ "setSpecie", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a613326c2eb0173d0549e21129e7f6ad4", null ],
    [ "setWingSize", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#aef76d173cafbe0795d1a18f94147c1d3", null ],
    [ "toString", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html#a896dad95d54c59ce27db37a21af5f578", null ]
];