var interfacesos_1_1us_1_1es_1_1_bird =
[
    [ "getEclosions", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a45446dc035fd2733ebcc559bfe1c08c9", null ],
    [ "getEggs", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a3be9a09ffb9618c7774324ce2958e423", null ],
    [ "getId", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a2dd62faa88be4132cdf6a75267ae3868", null ],
    [ "getLegDiameter", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a0c1c8ac75cd03de45deb152c64b3bff8", null ],
    [ "getPlace", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#af37eceaa2b776376d55574506e6fc370", null ],
    [ "getSpecie", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a758854ba8162f6fdfef5ba50d05206ac", null ],
    [ "getWingSize", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a2e9d300f829ef1d9c68ee7968802e49e", null ],
    [ "setEclosions", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a02e467bee604185497eebf6a36d4ffb6", null ],
    [ "setEggs", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a348b40c6f75ea28ae4ad015f909a0eae", null ],
    [ "setId", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#ac8c3cc98336b806fab9120ff02fcb4f1", null ],
    [ "setLegDiameter", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#abda1d403927ddd85119ed334325c3be1", null ],
    [ "setPlace", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a33f389282ba2685b9f9f102b1eeb2493", null ],
    [ "setSpecie", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a57f5f1653773a7269da348d25280b29f", null ],
    [ "setWingSize", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html#a93b71fdf10a7ec63e1100b6087879fd5", null ]
];