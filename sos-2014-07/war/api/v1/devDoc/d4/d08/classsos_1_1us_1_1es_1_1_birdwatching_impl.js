var classsos_1_1us_1_1es_1_1_birdwatching_impl =
[
    [ "BirdwatchingImpl", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a3485250e9a1113169827ac316738a04e", null ],
    [ "BirdwatchingImpl", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a71c437a3e010ab46fdd7f8b8cbdec62c", null ],
    [ "getCommonName", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#aea1ad781026e7ecec7bb68a8d88355f9", null ],
    [ "getDate", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a2486a9a17fcf8898a9e5761d6b03499b", null ],
    [ "getGUI", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#ad7ce926bae25043e8668ed048713fb1b", null ],
    [ "getLatitude", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a51a989df90db77f08077b16206cc0fca", null ],
    [ "getLongitude", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a48bcf3e492b22ddbddab6e49f3d35443", null ],
    [ "getSpecies", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#ad1209bd642949b4f996a22a2cbffaedd", null ],
    [ "getwatchSiteName", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a27fac5e1f874a22cc9dc43c39d98deb7", null ],
    [ "setCommonName", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#ad70eb12ddead7f7b05c45a51c59a25e1", null ],
    [ "setDate", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#aae88f6c1c3c644776179c6e99577cae0", null ],
    [ "setGUI", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a3cb7d9933b6658d8c43d2ba0e30e3c52", null ],
    [ "setLatitude", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a82b4f5822c3a98c3a79fe5d493a7fda9", null ],
    [ "setLongitude", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#ab3b50a824f4959c6e6d3d973e9776aba", null ],
    [ "setSpecies", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a57de9fbee8da65fbc890a310e5252b71", null ],
    [ "setwatchSiteName", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html#a848aa30f9cd7695eb6229024b5b94e58", null ]
];