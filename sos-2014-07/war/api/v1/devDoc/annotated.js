var annotated =
[
    [ "sos", null, [
      [ "us", null, [
        [ "es", null, [
          [ "Bird", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird.html", "d4/d75/interfacesos_1_1us_1_1es_1_1_bird" ],
          [ "Bird_servlet", "d0/d90/classsos_1_1us_1_1es_1_1_bird__servlet.html", "d0/d90/classsos_1_1us_1_1es_1_1_bird__servlet" ],
          [ "BirdImpl", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl.html", "dc/d5c/classsos_1_1us_1_1es_1_1_bird_impl" ],
          [ "Birds", "d6/d58/interfacesos_1_1us_1_1es_1_1_birds.html", "d6/d58/interfacesos_1_1us_1_1es_1_1_birds" ],
          [ "BirdsImpl", "dd/dea/classsos_1_1us_1_1es_1_1_birds_impl.html", "dd/dea/classsos_1_1us_1_1es_1_1_birds_impl" ],
          [ "Birdwatching", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching.html", "de/dc9/interfacesos_1_1us_1_1es_1_1_birdwatching" ],
          [ "Birdwatching_servlet", "de/d82/classsos_1_1us_1_1es_1_1_birdwatching__servlet.html", "de/d82/classsos_1_1us_1_1es_1_1_birdwatching__servlet" ],
          [ "BirdwatchingImpl", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl.html", "d4/d08/classsos_1_1us_1_1es_1_1_birdwatching_impl" ],
          [ "Location", "de/d7d/interfacesos_1_1us_1_1es_1_1_location.html", "de/d7d/interfacesos_1_1us_1_1es_1_1_location" ],
          [ "Location_servlet", "d1/d2d/classsos_1_1us_1_1es_1_1_location__servlet.html", "d1/d2d/classsos_1_1us_1_1es_1_1_location__servlet" ],
          [ "LocationImpl", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl.html", "d2/d6b/classsos_1_1us_1_1es_1_1_location_impl" ],
          [ "Servlet_0", "d2/d99/classsos_1_1us_1_1es_1_1_servlet__0.html", "d2/d99/classsos_1_1us_1_1es_1_1_servlet__0" ]
        ] ]
      ] ]
    ] ]
];