//////////////////////////////// <<<<INITIALIZATION>>>> ////////////////////////////////
var app = angular.module('myApp', ['ngGrid', 'ui.bootstrap']);

//////////////////////////////// <<<<SERVICES>>>> ////////////////////////////////
app.service('dataService', function($http) {
  delete $http.defaults.headers.common['X-Requested-With'];
  this.getData = function(resource) {
    return $http({
      method: 'GET',
      url: '/api/v1/' + resource,
    }).
    error(function(data, status, headers, config) {

      //alertify.error("GET "+status);
      if (status == "500") {
        alertify.error("Error while processing your request, try again or contact with the administrator.");
      }

    });
  }
  this.putData = function(resource, dataPut, id) {
    //alert("Sending PUT to id: " + id + ". Data: " + dataPut);
    return $http({
      method: 'PUT',
      url: '/api/v1/' + resource + '/' + id,
      data: dataPut
    }).
    error(function(data, status, headers, config) {

      //alertify.error("PUT "+status);
      if (status == "404") {
        alertify.error("It is not allowed to change bird ID, please, delete this record and add another again please.");
      }

      if (status == "500") {
        alertify.error("Error while processing your request, try again or contact with the administrator.");
      }

    });
  }
  this.postData = function(resource, dataPost) {
    //alert("Sending POST. Data: " + dataPost);
    return $http({
      method: 'POST',
      url: '/api/v1/' + resource,
      data: dataPost
    }).
    error(function(data, status, headers, config) {

      //alertify.error("POST "+status);
      if (status == "409") {
        alertify.error("A bird with this ID already exists in the database. Please, check your input.");
      }

      if (status == "500") {
        alertify.error("Error while processing your request, try again or contact with the administrator.");
      }

    });
  }
  this.deleteData = function(resource, id) {
    //alert("Sending DELETE to id: " + id);
    return $http({
      method: 'DELETE',
      url: '/api/v1/' + resource + '/' + id
    }).
    error(function(data, status, headers, config) {

      //alertify.error("DELETE "+status);
      if (status == "500") {
        alertify.error("Error while processing your request, try again or contact with the administrator.");
      }

    });
  }
});



//////////////////////////////// <<<<DIRECTIVES>>>> ////////////////////////////////
// Creating modal pop-up
app.directive('igLogin', function() {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: "modal.html",
    controller: function($scope) {
      $scope.cancel = function() {
        $scope.formDisplayed = false;
        $("#loginModal").modal('hide');
        $scope.resetForm();
      };
      $scope.$watch('formDisplayed', function() {
        if ($scope.formDisplayed) {
          $("#loginModal").modal('show');
        };
      });
    }
  };
});
// EXPERIMENTAL FEATURE, PLEASE CHECK MODAL.HTML
// CURRTENTLY, IT IS NOT WORKING
// Creating custom validation: float (comma and dot)
var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
app.directive('smartFloat', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (FLOAT_REGEXP.test(viewValue)) {
          ctrl.$setValidity('float', true);
          return parseFloat(viewValue.replace(',', '.'));
        } else {
          ctrl.$setValidity('float', false);
          return undefined;
        }
      });
    }
  };
});
// Creating custom validation: integer
var INTEGER_REGEXP = /^\-?\d*$/;
app.directive('integer', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (INTEGER_REGEXP.test(viewValue)) {
          // it is valid
          ctrl.$setValidity('integer', true);
          return viewValue;
        } else {
          // it is invalid, return undefined (no model update)
          ctrl.$setValidity('integer', false);
          return undefined;
        }
      });
    }
  };
});

//////////////////////////////// <<<<CONTROLLER>>>> ////////////////////////////////
app.controller('MyCtrl', function($scope, dataService) {


  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.showWeeks = true;
  $scope.toggleWeeks = function() {
    $scope.showWeeks = !$scope.showWeeks;
  };

  $scope.clear = function() {
    $scope.location.date = null;
    $scope.watchings.date = null;
    $scope.birds.date = null;
  };

  $scope.toggleMin = function() {
    $scope.minDate = ($scope.minDate) ? null : new Date();
  };
  $scope.toggleMin();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened1 = true;
    $scope.opened2 = true;
  };

  $scope.dateOptions = {
    'year-format': "'yy'",
    'starting-day': 1
  };


  //TESTS
  $scope.resource = "birds"; //default resource
  $scope.cols = [{
    field: 'id',
    displayName: 'Bird ID',
    enableCellEdit: false
  }, {
    field: 'specie',
    displayName: 'Specie',
    enableCellEdit: true
  }, {
    field: 'place',
    displayName: 'Place',
    enableCellEdit: true
  }, {
    field: 'legDiameter',
    displayName: 'Leg Diameter',
    enableCellEdit: true
  }, {
    field: 'wingSize',
    displayName: 'Wing Size',
    enableCellEdit: true
  }, {
    field: 'eggs',
    displayName: 'Eggs',
    enableCellEdit: true
  }, {
    field: 'eclosions',
    displayName: 'Hatches',
    enableCellEdit: true
  }];

  $scope.changeModel = function(resource) {
    $scope.resource = resource;
    $scope.popup = 'modal.html';
    //console.log($scope);

    dataService.getData(resource).then(function(dataResponse) {
      $scope.myData = dataResponse.data;
    });
    if (resource == "birds") {
      $scope.cols = [{
        field: 'id',
        displayName: 'Bird ID',
        enableCellEdit: false
      }, {
        field: 'specie',
        displayName: 'Specie',
        enableCellEdit: true
      }, {
        field: 'place',
        displayName: 'Place',
        enableCellEdit: true
      }, {
        field: 'legDiameter',
        displayName: 'Leg Diameter',
        enableCellEdit: true
      }, {
        field: 'wingSize',
        displayName: 'Wing Size',
        enableCellEdit: true
      }, {
        field: 'eggs',
        displayName: 'Eggs',
        enableCellEdit: true
      }, {
        field: 'eclosions',
        displayName: 'Hatches',
        enableCellEdit: true
      }];
    }

    if (resource == "watchings") {
      $scope.cols = [{
        field: 'GUI',
        displayName: 'GUI',
        enableCellEdit: false
      }, {
        field: 'species',
        displayName: 'Specie',
        enableCellEdit: true
      }, {
        field: 'commonName',
        displayName: 'Common Name',
        enableCellEdit: true
      }, {
        field: 'watchSiteName',
        displayName: 'Place of Watching',
        enableCellEdit: true
      }, {
        field: 'latitude',
        displayName: 'Latitude',
        enableCellEdit: true
      }, {
        field: 'longitude',
        displayName: 'Longitude',
        enableCellEdit: true
      }, {
        field: 'date',
        displayName: 'Date',
        enableCellEdit: true
      }];
    }

    if (resource == "locations") {
      $scope.cols = [{
        field: 'id',
        displayName: 'ID',
        enableCellEdit: false
      }, {
        field: 'place',
        displayName: 'Place',
        enableCellEdit: true
      }, {
        field: 'nvdi',
        displayName: 'NVDI',
        enableCellEdit: true
      }, {
        field: 'temp',
        displayName: 'Temperature',
        enableCellEdit: true
      }, {
        field: 'date',
        displayName: 'Date',
        enableCellEdit: true
      }];
    }

  }

  //END_TESTS


  //////////////////////////////// GET ////////////////////////////////

  dataService.getData($scope.resource).then(function(dataResponse) {
    $scope.myData = dataResponse.data;

  });

  //////////////////////////////// END_GET ////////////////////////////////
  //////////////////////////////// TABLE_VISUALIZATION ////////////////////////////////
  // Selection for bulk-actions
  $scope.mySelections = [];
  // Client-Side Filtering
  $scope.filterOptions = {
    filterText: ''

  };
  // Pagination 
  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
    pageSizes: [10, 50, 100, 250],
    pageSize: 10,
    currentPage: 1,
    totalServerItems: 0
  };
  $scope.setPagingData = function(data, page, pageSize) {
    var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
    $scope.myData = pagedData;
    $scope.totalServerItems = data.length;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };
  $scope.getPagedDataAsync = function(pageSize, page, searchText) {

    data = ''
    if (searchText) {

      console.log(searchText);
      dataService.getData($scope.resource).then(function(dataResponse) {
        $scope.myData = dataResponse.data;
      });

    } else {
      // GET Service-calling paginated
      dataService.getData($scope.resource).then(function(dataResponse) {
        data = dataResponse.data;
        $scope.setPagingData(data, page, pageSize);
      });
    }
  };
  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);


  $scope.$watch('pagingOptions', function(newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, null);
    }
  }, true);

  $scope.$watch('filterOptions', function(newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
  }, true);

  // End Of Pagination
  // GET Service-calling
  // Options from Grid 
  $scope.gridOptions = {
    data: 'myData',
    jqueryUITheme: true,
    selectedItems: $scope.mySelections,
    filterOptions: $scope.filterOptions,
    enableCellSelection: true,
    multiSelect: true,
    showSelectionCheckbox: true,
    // Pagination
    enablePaging: true,
    showFooter: true,
    totalServerItems: 'totalServerItems',
    pagingOptions: $scope.pagingOptions,
    //
    columnDefs: 'cols'
  };



  //////////////////////////////// END_TABLE_VISUALIZATION ////////////////////////////////
  //////////////////////////////// UPDATE ////////////////////////////////
  $scope.$on("ngGridEventEndCellEdit", function(evt) {
    // Put data when a cell change its state
    var entity = evt.targetScope.row.entity;
    var json = JSON.stringify(evt.targetScope.row.entity);
    var id = "";
    if ($scope.resource == "birds") {
      id = entity.id;
    }
    if ($scope.resource == "watchings") {
      id = entity.GUI;
    }
    if ($scope.resource == "locations") {
      id = entity.id;
    }

    dataService.putData($scope.resource, json, id);
    alertify.success("Row successfully updated");
  });

  //////////////////////////////// END_UPDATE ////////////////////////////////
  //////////////////////////////// DELETE ////////////////////////////////
  $scope.removeSelectedRows = function() { // Read from $scope the rows that have been selected 
    angular.forEach($scope.mySelections, function(rowItem) {
      var id = "";
      if ($scope.resource == "birds") {
        id = rowItem['id'];
      }
      if ($scope.resource == "watchings") {
        id = rowItem['GUI'];
      }
      if ($scope.resource == "locations") {
        id = rowItem['id'];
      }

      console.log("Value to delete : " + id);
      dataService.deleteData($scope.resource, id);
      $scope.myData.splice($scope.myData.indexOf(rowItem), 1);

    });
    alertify.success("Row(s) successfully deleted");
  };

  $scope.removeAll = function() {
    // Instead of using a loop iterating over all rows, it uses API DELETE design.
    dataService.deleteData($scope.resource, '');
    $scope.myData = [];
    alertify.success("All rows have been successfully deleted");
  };

  //////////////////////////////// END_DELETE ////////////////////////////////
  //////////////////////////////// CREATE ////////////////////////////////
  $scope.resetForm = function() {
    //reseting all fields in the main form
    $('#birdForm').each(function() {
      this.reset();
    });
  }

  $scope.formDisplayed = false;
  $scope.showForm = function() {
    $scope.resetForm();
    $("#loginModal").modal('show');
    $scope.formDisplayed = true;
  };
  $scope.hideForm = function() {
    $scope.resetForm();
    $("#loginModal").modal('hide');
    $scope.formDisplayed = false;
  };
  $scope.addRowForm = function() {
    alertify.set({
      delay: 4000
    });
    // checking error in the form
  // HERE GOES IF
  var thirdDeliverable = true;
      // hide and clear form
      $("#loginModal").modal('hide');
      $scope.resetForm();

      if ($scope.resource == "birds") {
        $scope.addBird($scope.bird["id"], $scope.bird["specie"], $scope.bird["place"], $scope.bird["legDiameter"], $scope.bird["wingSize"], $scope.bird["eggs"], $scope.bird["eclosions"]);
      }
      if ($scope.resource == "watchings") {
        $scope.addWatching($scope.watching["GUI"], $scope.watching["species"], $scope.watching["commonName"], $scope.watching["watchSiteName"], $scope.watching["latitude"], $scope.watching["longitude"], $scope.watching["date"]);
      }
      if ($scope.resource == "locations") {
        $scope.addLocation($scope.location["id"], $scope.location["place"], $scope.location["nvdi"], $scope.location["temp"], $scope.location["date"]);
      }

  if ($scope.birdForm.$valid) {
      alertify.success("Row successfully added");
      var a = $filter('date')($scope.location["date"],'yyyy-MM-dd');
      console.log(a);
    } else if (thirdDeliverable) {
      alertify.warning("Row added");
      console.log($scope.birdForm);
      console.log($scope.location["date"]);
    }else{
      alertify.error("Please check your input");
      console.log($scope.birdForm);
    };
  };

  $scope.addBird = function(id_p, specie_p, place_p, legDiameter_p, wingSize_p, eggs_p, eclosions_p) {
    // Add a row at either table or server
    var json = {
      id: id_p,
      specie: specie_p,
      place: place_p,
      legDiameter: legDiameter_p,
      wingSize: wingSize_p,
      eggs: eggs_p,
      eclosions: eclosions_p
    };
    // POST call
    dataService.postData($scope.resource, json);
    // Display in the table
    $scope.myData.push(json);
  };

  $scope.addWatching = function(GUI_p, species_p, commonName_p, watchSiteName_p, latitude_p, longitude_p, date_p) {
    // Add a row at either table or server
    var json = {
      GUI: GUI_p,
      species: species_p,
      commonName: commonName_p,
      watchSiteName: watchSiteName_p,
      latitude: latitude_p,
      longitude: longitude_p,
      date: date_p
    };
    // POST call
    dataService.postData($scope.resource, json);
    // Display in the table
    $scope.myData.push(json);
  };

  $scope.addLocation = function(id_p, place_p, nvdi_p, temp_p, date_p) {
    // Add a row at either table or server
    var json = {
      id: id_p,
      place: place_p,
      nvdi: nvdi_p,
      temp: temp_p,
      date: date_p
    };
    // POST call
    dataService.postData($scope.resource, json);
    // Display in the table
    $scope.myData.push(json);
  };
});
//////////////////////////////// END_CREATE ////////////////////////////////