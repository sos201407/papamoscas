package sos.us.es;

//~--- non-JDK imports --------------------------------------------------------

//~--- JDK imports ------------------------------------------------------------
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/*
 * To be implemented: Add security by requiring an API key to use our API. OAuth? Tokens? 
 */

@SuppressWarnings("serial")
public class Bird_servlet extends HttpServlet {
    public static Gson             g  = new Gson();
    public static DatastoreService db = DatastoreServiceFactory.getDatastoreService();

    // public static DatastoreService auth     = DatastoreServiceFactory.getDatastoreService();
    //public List<Bird> birdList = new LinkedList<>();

   
    /*public void init() {
    	// Only for testing purposes, to be removed at release version
        Bird b1 = new BirdImpl("ABC", "papamosca", "roble", (float) 0.1, (float) 10.0, 10, 3);
        Bird b2 = new BirdImpl("BCD", "vencejo", "pino", (float) 0.15, (float) 9.0, 7, 5);
        Bird b3 = new BirdImpl("EFG", "papamosca", "pino", (float) 0.1, (float) 10.0, 10, 3);
        Bird b4 = new BirdImpl("HIJ", "vencejo", "roble", (float) 0.15, (float) 9.0, 7, 5);

        birdList.add(b1);
        birdList.add(b2);
        birdList.add(b3);
        birdList.add(b4);

        for (Bird b : birdList) {
            saveBirdToDatastore(b);
        }

        // saveSecureKey("noQuieroQueUsesMiApiYMeGastesMisRecursos");
    }*/

    /*
     *  private void saveSecureKey(String key) {
     *    Entity e = new Entity(apiKeyImpl, key);
     *
     *    auth.put(e);
     * }
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        g = new Gson();
        resp.setCharacterEncoding("UTF-8");
        //init();    // Only for testing purposes, to be removed at release version
        g = new Gson();

        String       json         = "";
        List<Filter> filterList   = new ArrayList<>();
        String       resourcePath = req.getPathInfo();

        try {

            // check if it's a collection or a single resource
            if ((resourcePath != null) &&!resourcePath.equals("/")) {
                String[] resources = resourcePath.split("/");

                // it only accepts URI's like: /birds/birdID
                if (resources.length > 2) {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                } else {
                    String birdId = resources[1];

                    if (!req.getParameterMap().isEmpty()) {

                        // URI contains parameters
                        filterList.add(new FilterPredicate("id", FilterOperator.EQUAL, birdId));
                        handleGetWithParameters(req, resp, filterList);
                    } else {    // URI doesn't contain parameters
                        Query q    = new Query("bird").setFilter(new FilterPredicate("id", FilterOperator.EQUAL,
                                         birdId));
                        Bird  bird = getBirdfromDatastoreQuery(q);

                        // check if query has data to show
                        if (bird != null) {
                            json = g.toJson(bird);
                        } else {
                            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                        }
                    }
                }
            } else {            // it's the entire collection
                if (!req.getParameterMap().isEmpty()) {

                    // URI contains parameters
                    handleGetWithParameters(req, resp, filterList);
                } else {    // URI doesn't contain parameters
                    Query      q        = new Query("bird");
                    List<Bird> birdList = getListfromDatastoreQuery(q);

                    // check if query has data to show
                    if (!birdList.isEmpty()) {
                        json = g.toJson(birdList);
                    } else {
                        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    }
                }
            }

            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write(json);
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
    }

    @Override
    public void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        g = new Gson();
        resp.setCharacterEncoding("UTF-8");

        try {
            String resourcePath = req.getPathInfo();

            // check if it's a collection or a single resource
            if ((resourcePath != null) &&!resourcePath.equals("/")) {
                String[] resources = resourcePath.split("/");

                // it only accepts URI's like: /birds/birdID
                if (resources.length > 2) {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                } else {
                    String        birdId = resources[1];
                    Query         q      = new Query("bird").setFilter(new FilterPredicate("id", FilterOperator.EQUAL,
                                               birdId));
                    PreparedQuery pq     = db.prepare(q);

                    // check if query has data to show
                    try {
                        Entity e = pq.asSingleEntity();
                                              
                        
                        if ((e != null) && e.getProperty("id").equals(birdId)) {
                            // bird exists and id's match
                        	db.delete(e.getKey());
                            Bird b = jsonToBird(req);
                            saveBirdToDatastore(b);
                            resp.setStatus(HttpServletResponse.SC_OK);
                            /*
                            if (!b.getId().equalsIgnoreCase(birdId)) {
                            	// if it's an incorrect id, remove the entity created before
                            	System.out.println(e);
                            	db.delete(e.getKey()); 
                                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            } else {
                                saveBirdToDatastore(b);
                                resp.setStatus(HttpServletResponse.SC_OK);    // ok
                            }*/
                        } else {
                            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }
                }
            } else {                                                          // it's the entire collection
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
    }

    @Override
    public void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        g = new Gson();
        resp.setCharacterEncoding("UTF-8");

        try {
            String resourcePath = req.getPathInfo();

            // check if it's a collection or a single resource
            if ((resourcePath != null) &&!resourcePath.equals("/")) {
                String[] resources = resourcePath.split("/");

                // it only accepts URI's like: /birds/birdID
                if (resources.length > 2) {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                } else {
                    String        birdId = resources[1];
                    Query         q      = new Query("bird").setFilter(new FilterPredicate("id", FilterOperator.EQUAL,
                                               birdId));
                    PreparedQuery pq     = db.prepare(q);

                    // check if query has data to show
                    try {
                        Entity e = pq.asSingleEntity();

                        if (e != null) {

                            // as we use birdId as identifier, we could also
                            // delete by using URI parameter
                            db.delete(e.getKey());
                            resp.setStatus(HttpServletResponse.SC_NO_CONTENT);    // ok
                        } else {
                            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);     // hide

                            // others
                            // not-catched
                            // problems
                        }
                    } catch (Exception e) {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }
                }
            } else {                                                              // it's the entire collection
                Query         q  = new Query("bird");
                PreparedQuery pq = db.prepare(q);

                // exceptions here are not being catched beacause it only throws
                // server derivated errors (caught below)
                for (Entity e : pq.asIterable()) {
                    db.delete(e.getKey());
                }

                resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        g = new Gson();
        resp.setCharacterEncoding("UTF-8");

        String resourcePath = req.getPathInfo();

        try {

            // check if it's a collection or a single resource
            if ((resourcePath != null) &&!resourcePath.equals("/")) {

                // single resource POST is not allowed
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            } else {
                StringBuilder  sb = new StringBuilder();
                BufferedReader br = req.getReader();
                String         jsonString;

                while ((jsonString = br.readLine()) != null) {
                    sb.append(jsonString);
                }

                jsonString = sb.toString();

                Bird b = g.fromJson(jsonString, BirdImpl.class);

                if (!checkIfBirdExists(b)) {

                    // the bird doesn't exist
                    // DONE: now it's only works with ONE BIRD
                    // it seems it the proper way to do that :)
                    saveBirdToDatastore(b);
                    resp.setStatus(HttpServletResponse.SC_CREATED);
                } else {

                    // the bird exists
                    resp.setStatus(HttpServletResponse.SC_CONFLICT);
                }
            }
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
    }

    private void handleGetWithParameters(HttpServletRequest req, HttpServletResponse resp, List<Filter> filterList)
            throws IOException {
        Query                                              q              = new Query("bird");
        String                                             json           = "";
        @SuppressWarnings("unchecked") Enumeration<String> parameterNames = req.getParameterNames();

        filterList.add(new FilterPredicate("id", FilterOperator.NOT_EQUAL, ""));    // true

        while (parameterNames.hasMoreElements()) {
            String   paramName   = parameterNames.nextElement();
            String[] paramValues = req.getParameterValues(paramName);

            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];

                switch (paramName) {
                case "fields" :
                    final List<String> fields = Lists.newArrayList(paramValue.split(","));

                    filterList.add(new FilterPredicate("id", FilterOperator.NOT_EQUAL, ""));    // true

                    // filter
                    // We need to modify the gson builder in
                    // order to filter desired fields
                    GsonBuilder       gsonBuilder = new GsonBuilder();
                    ExclusionStrategy exclusion   = new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {

                            // if true -> skip field,
                            // so returns true IF f IS NOT
                            // contained in parameters list
                            boolean a = f.getDeclaringClass() == BirdImpl.class;
                            boolean b = !fields.contains(f.getName());

                            return a && b;
                        }
                        @Override
                        public boolean shouldSkipClass(Class<?> arg0) {
                            return false;
                        }
                    };

                    gsonBuilder.setExclusionStrategies(exclusion);
                    g = gsonBuilder.create();

                    break;

                case "specie" :
                    String specie = paramValue.split(",")[0];

                    filterList.add(new FilterPredicate("specie", FilterOperator.EQUAL, specie.toLowerCase()));

                    break;

                case "place" :
                    String place = paramValue.split(",")[0];

                    filterList.add(new FilterPredicate("place", FilterOperator.EQUAL, place.toLowerCase()));

                    break;

                /*
                 * case "key" :
                 *   String key = paramValue.split(",")[0];
                 *
                 *   if (!checkApiKey(key)) {
                 *       resp.sendError(HttpServletResponse.SC_FORBIDDEN);
                 *   }
                 *
                 *   break;
                 */
                default :
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                    break;
                }
            }
        }

        try {

            // note that CompositeFilter needs TWO filters (at
            // least) to work, so
            // a always true filter has been added above.
            CompositeFilter ff = new CompositeFilter(CompositeFilterOperator.AND, filterList);

            q.setFilter(ff);

            List<Bird> birdList = getListfromDatastoreQuery(q);

            // check if query has data to show
            if (!birdList.isEmpty()) {
                json = g.toJson(birdList);    // g should have been

                // modified at this
                // point if parameter
                // "fields" was enabled.
            } else {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(json);
    }

    private void saveBirdToDatastore(Bird b) {
        Entity e = new Entity("bird", b.getId());

        e.setProperty("id", b.getId());
        e.setProperty("specie", b.getSpecie());
        e.setProperty("place", b.getPlace());
        e.setProperty("legDiameter", new Float(b.getLegDiameter()));
        e.setProperty("wingSize", new Float(b.getWingSize()));
        e.setProperty("eggs", new Integer(b.getEggs()));
        e.setProperty("eclosions", new Integer(b.getEclosions()));
        db.put(e);
    }

    private Bird jsonToBird(HttpServletRequest req) throws IOException {
        StringBuilder  sb = new StringBuilder();
        BufferedReader br = req.getReader();
        String         jsonString;

        while ((jsonString = br.readLine()) != null) {
            sb.append(jsonString);
        }

        jsonString = sb.toString();

        Bird b = g.fromJson(jsonString, BirdImpl.class);

        saveBirdToDatastore(b);

        return b;
    }

    private List<Bird> getListfromDatastoreQuery(Query q) {
        List<Bird>    birdList = new LinkedList<>();
        PreparedQuery pq       = db.prepare(q);

        for (Entity ent : pq.asIterable()) {

            // problems during casting from Double/Long to Float/Integer because
            // of the way to save the entity properties into datastore
            Bird b = new BirdImpl(ent.getProperty("id").toString(), ent.getProperty("specie").toString(),
                                  ent.getProperty("place").toString(),
                                  new Float(ent.getProperty("legDiameter").toString()),
                                  new Float(ent.getProperty("wingSize").toString()),
                                  new Integer(ent.getProperty("eggs").toString()),
                                  new Integer(ent.getProperty("eclosions").toString()));

            birdList.add(b);
        }

        return birdList;
    }

    private Bird getBirdfromDatastoreQuery(Query q) {
        try {
            PreparedQuery pq  = db.prepare(q);
            Entity        ent = pq.asSingleEntity();

            // problems during casting from Double/Long to Float/Integer because
            // of the way to save the entity properties into datastore
            Bird b = new BirdImpl(ent.getProperty("id").toString(), ent.getProperty("specie").toString(),
                                  ent.getProperty("place").toString(),
                                  new Float(ent.getProperty("legDiameter").toString()),
                                  new Float(ent.getProperty("wingSize").toString()),
                                  new Integer(ent.getProperty("eggs").toString()),
                                  new Integer(ent.getProperty("eclosions").toString()));

            return b;
        } catch (Exception e) {

            // the bird does not exist
            return null;
        }
    }

    private Boolean checkIfBirdExists(Bird b) {
        Query q = new Query("bird").setFilter(new FilterPredicate("id", FilterOperator.EQUAL, b.getId()));

        return getBirdfromDatastoreQuery(q) != null;
    }

    /*
     * private Boolean checkApiKey(String key) {
     *    PreparedQuery pq = auth.prepare(new Query("key").setFilter(new FilterPredicate("id", FilterOperator.EQUAL,
     *                           key)));
     *    Entity ent = pq.asSingleEntity();
     *
     *    return key.equals(ent.getProperty("key"));
     * }
     */
}


//~ Formatted by Jindent --- http://www.jindent.com
