package sos.us.es;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

public class BirdImpl implements Bird, Serializable {
    private static final long serialVersionUID = -3235538169360094210L;
    private String            id;
    private String            specie;
    private String            place;
    private Float             legDiameter;
    private Float             wingSize;
    private Integer           eggs;
    private Integer           eclosions;

    /**
     * @param id
     * @param specie
     * @param place
     * @param legDiameter
     * @param wingSize
     * @param eggs
     * @param eclosions
     */
    public BirdImpl() {
        super();
    }

    public BirdImpl(String id, String specie, String place, Float legDiameter, Float wingSize, Integer eggs,
                    Integer eclosions) {
        super();
        this.id          = id;
        this.specie      = specie;
        this.place       = place;
        this.legDiameter = legDiameter;
        this.wingSize    = wingSize;
        this.eggs        = eggs;
        this.eclosions   = eclosions;
    }

    @Override
    public String getSpecie() {
        return specie;
    }

    @Override
    public void setSpecie(String specie) {
        this.specie = specie;
    }

    @Override
    public String getPlace() {
        return place;
    }

    @Override
    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public Float getLegDiameter() {
        return legDiameter;
    }

    @Override
    public void setLegDiameter(Float legDiameter) {
        this.legDiameter = legDiameter;
    }

    @Override
    public Float getWingSize() {
        return wingSize;
    }

    @Override
    public void setWingSize(Float wingSize) {
        this.wingSize = wingSize;
    }

    @Override
    public Integer getEggs() {
        return eggs;
    }

    @Override
    public void setEggs(Integer eggs) {
        this.eggs = eggs;
    }

    @Override
    public Integer getEclosions() {
        return eclosions;
    }

    @Override
    public void setEclosions(Integer eclosions) {
        this.eclosions = eclosions;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BirdImpl [specie=" + specie + ", place=" + place + ", legDiameter=" + legDiameter + ", wingSize="
               + wingSize + ", eggs=" + eggs + ", eclosions=" + eclosions + ", id=" + id + "]";
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
