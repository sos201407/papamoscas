package sos.us.es;

public interface Location {
    public abstract void setDate(String date);

    public abstract String getDate();

    public abstract void setPlace(String place);

    public abstract String getPlace();

    public abstract void setNVDI(Float nvdi);

    public abstract Float getNVDI();

    public abstract void setTemp(Float temp);

    public abstract Float getTemp();

    public abstract void setID(String ID);

    public abstract String getID();
}


//~ Formatted by Jindent --- http://www.jindent.com
