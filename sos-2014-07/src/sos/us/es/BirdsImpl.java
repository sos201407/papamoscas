package sos.us.es;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

import java.util.List;

public class BirdsImpl implements Birds, Serializable {
    private static final long serialVersionUID = 2713631620283779558L;
    private List<Bird>        birds;

    /**
     * @param birds
     */
    public BirdsImpl() {
        super();
    }

    public BirdsImpl(List<Bird> birds) {
        super();
        this.birds = birds;
    }

    @Override
    public List<Bird> getBirds() {
        return birds;
    }

    @Override
    public void setBirds(List<Bird> birds) {
        this.birds = birds;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
