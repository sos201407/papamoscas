package sos.us.es;

public interface Birdwatching {
    public abstract String getGUI();

    public abstract void setGUI(String GUI);

    public abstract String getSpecies();

    public abstract void setSpecies(String species);

    public abstract String getCommonName();

    public abstract void setCommonName(String commonName);

    public abstract String getwatchSiteName();

    public abstract void setwatchSiteName(String watchSiteName);

    public abstract Double getLatitude();

    public abstract void setLatitude(Double latitude);

    public abstract Double getLongitude();

    public abstract void setLongitude(Double longitude);

    public abstract String getDate();

    public abstract void setDate(String date);
}


//~ Formatted by Jindent --- http://www.jindent.com
