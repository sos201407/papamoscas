package sos.us.es;

//~--- non-JDK imports --------------------------------------------------------

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//~--- JDK imports ------------------------------------------------------------
import java.io.BufferedReader;

@SuppressWarnings("serial")
public class Birdwatching_servlet extends HttpServlet {
    public static List<Birdwatching> wathchingList = new LinkedList<Birdwatching>();
    public static Gson               gstream       = new Gson();
    public static DatastoreService   dataBase      = DatastoreServiceFactory.getDatastoreService();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");

        // Default vars
        List<Filter> filterList = new LinkedList<Filter>();
        String       json       = null;
        List<String> uriItems   = null;

        try {
            String resourcePath = req.getPathInfo();

            // Check if the resource path is null. If not extract uri items
            if (resourcePath != null) {
                String[] uri = resourcePath.split("/");

                uriItems = Arrays.asList(uri);
            }

            // If there are not arguments, GET ALL
            if ((resourcePath == null) || (uriItems.size() < 2)) {

                // CheckParams Before
                // If there are not params, return All
                if (req.getParameterNames() == null) {
                    Query              q                  = new Query("watching");
                    PreparedQuery      pq                 = dataBase.prepare(q);
                    List<Birdwatching> containerBirdWatch = new LinkedList<>();

                    // Get elements as entity with Iterable
                    for (Entity ent : pq.asIterable()) {
                        containerBirdWatch.add(processEntityToWatching(ent));
                    }

                    // Check if exist results. TODO add else with SC_NOT_FOUND
                    if (containerBirdWatch.size() > 0) {
                        json = gstream.toJson(containerBirdWatch);
                        resp.setStatus(HttpServletResponse.SC_OK);
                        resp.setContentType("application/json");
                        resp.setCharacterEncoding("UTF-8");
                        resp.getWriter().write(json);
                    }
                } else {

                    // If there are params
                    doGetWithParameters(req, resp, filterList);
                }
            } else if ((resourcePath != null) && (uriItems.size() > 2)) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }

            // Get ONE With Params ( by default )
            else {
                String watchinGUI = uriItems.get(1);

                filterList.add(new FilterPredicate("GUI", FilterOperator.EQUAL, watchinGUI));
                doGetWithParameters(req, resp, filterList);
            }
        } catch (Exception e) {
            System.out.println("Error ! " + e.getMessage());
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        // setUp();
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");

        List<String> uriItems     = null;
        String       resourcePath = req.getPathInfo();

        // Check if the url is null ( api/v1/ )
        if (resourcePath != null) {
            String[] uri = resourcePath.split("/");

            uriItems = Arrays.asList(uri);
        }

        // Check if there single or multiple vars. Forbidden in PU T
        if ((uriItems == null) || (uriItems.size() < 2) || (uriItems.size() > 2)) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {

            // Try query
            try {
                String watchinGUI = uriItems.get(1);

                System.out.println("GUI consulta :" + watchinGUI);

                Query q = new Query("watching").setFilter(new FilterPredicate("GUI", Query.FilterOperator.EQUAL,
                              watchinGUI));

                System.out.print(q);

                PreparedQuery pq = dataBase.prepare(q);

                System.out.print(pq);

                Entity ent = pq.asSingleEntity();

                System.out.println("GUI elemento:" + ent);

                // Check if the object exists and if is the right one
                if ((ent != null) && ent.getProperty("GUI").equals(watchinGUI)) {
                    dataBase.delete(ent.getKey());

                    Birdwatching birdWatchTemp = jsonToWatching(req);
                    Entity       entUpdate     = processWatchingToEntity(birdWatchTemp);

                    dataBase.put(entUpdate);
                    resp.setStatus(HttpServletResponse.SC_OK);
                } else {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            } catch (Exception e) {
                System.out.println("Error ! " + e.getMessage());
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
    }

    @Override
    public void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");

        List<String> uriItems = null;

        try {
            String resourcePath = req.getPathInfo();

            // Check if the url is null ( api/v1/ )
            if (resourcePath != null) {
                String[] uri = resourcePath.split("/");

                uriItems = Arrays.asList(uri);
            }

            // If there are not arguments, delete all
            if ((resourcePath == null) || (uriItems.size() < 2)) {
                Query         q  = new Query("watching");
                PreparedQuery pq = dataBase.prepare(q);

                for (Entity ent : pq.asIterable()) {
                    dataBase.delete(ent.getKey());
                }

                resp.setStatus(HttpServletResponse.SC_OK);
            }

            // Delete an unique row
            else {
                String watchinGUI = uriItems.get(1);
                Query  q          = new Query("watching").setFilter(new FilterPredicate("GUI",
                                        Query.FilterOperator.EQUAL, watchinGUI));
                PreparedQuery pq  = dataBase.prepare(q);
                Entity        ent = pq.asSingleEntity();

                if (ent != null) {
                    dataBase.delete(ent.getKey());
                    resp.setStatus(HttpServletResponse.SC_OK);
                } else {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        } catch (Exception e) {
            System.out.println("Error ! " + e.getMessage());
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");

        List<String> uriItems     = null;
        String       resourcePath = req.getPathInfo();

        // Check if the url is null ( api/v1/ )
        if (resourcePath != null) {
            String[] uri = resourcePath.split("/");

            uriItems = Arrays.asList(uri);
        }

        // Check if there are vars. Forbidden in POST
        if ((uriItems != null) && (uriItems.size() > 1)) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            try {
                Birdwatching birdWatchTemp = jsonToWatching(req);

                if (testElementExists(birdWatchTemp)) {
                    resp.setStatus(HttpServletResponse.SC_CONFLICT);
                    System.out.println(" Warning: An element in database already matches this ID ");

                    // throw new
                    // IllegalWriteException("Warning: An element in database already matches this ID ");
                } else {
                    Entity ent = processWatchingToEntity(birdWatchTemp);

                    dataBase.put(ent);
                    resp.setStatus(HttpServletResponse.SC_CREATED);
                }
            } catch (Exception e) {
                System.out.println("Error ! " + e.getMessage());
                resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }
    }

    private Boolean testElementExists(Birdwatching bw) {
        Query         q   = new Query("watching").setFilter(new FilterPredicate("GUI", Query.FilterOperator.EQUAL,
                                bw.getGUI()));
        PreparedQuery pq2 = dataBase.prepare(q);

        return pq2.asIterable().iterator().hasNext();
    }

    private Birdwatching jsonToWatching(HttpServletRequest req) throws IOException {
        StringBuilder  sb         = new StringBuilder();
        BufferedReader br         = req.getReader();
        String         jsonString = sb.toString();

        while ((jsonString = br.readLine()) != null) {
            sb.append(jsonString);
        }

        jsonString = sb.toString();

        Birdwatching bw = gstream.fromJson(jsonString, BirdwatchingImpl.class);

        return bw;
    }

    private Entity processWatchingToEntity(Birdwatching birdWatchTemp) {
        Entity e = new Entity("watching", birdWatchTemp.getGUI());

        e.setProperty("GUI", birdWatchTemp.getGUI());
        e.setProperty("commonName", birdWatchTemp.getCommonName());
        e.setProperty("species", birdWatchTemp.getSpecies());
        e.setProperty("watchSiteName", birdWatchTemp.getwatchSiteName());
        e.setProperty("latitude", birdWatchTemp.getLatitude());
        e.setProperty("longitude", birdWatchTemp.getLongitude());
        e.setProperty("date", birdWatchTemp.getDate());

        return e;
    }

    private Birdwatching processEntityToWatching(Entity e) {
        String GUI           = (String) e.getProperty("GUI");
        String commonName    = (String) e.getProperty("commonName");
        String species       = (String) e.getProperty("species");
        String watchSiteName = (String) e.getProperty("watchSiteName");
        String date          = (String) e.getProperty("date");
        Double longitude     = new Double((double) e.getProperty("longitude"));
        Double latitude      = new Double((double) e.getProperty("latitude"));

        return new BirdwatchingImpl(GUI, species, commonName, watchSiteName, latitude, longitude, date);
    }

    private void doGetWithParameters(HttpServletRequest req, HttpServletResponse resp, List<Filter> filterList)
            throws IOException {
        String                                             json   = "";
        Query                                              q      = new Query("watching");
        @SuppressWarnings("unchecked") Enumeration<String> params = req.getParameterNames();
        
        resp.addHeader("Access-Control-Allow-Origin", "*");
        for (String parameter : Collections.list(params)) {

            // Get list of Parameters' name and it values.
            String[] parameterValue = req.getParameterValues(parameter);
            String   value          = Arrays.toString(parameterValue).replace("[", "").replace("]", "");

            // If the parameters is fields, apply custom veiw
            if (parameter.equals("fields")) {

                //
                final List<String> fields    = Lists.newArrayList(value.split(","));
                ExclusionStrategy  exclusion = new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {

                        // if true -> skip field,
                        // so returns true IF f IS NOT
                        // contained in parameters list
                        boolean a = f.getDeclaringClass() == BirdwatchingImpl.class;
                        boolean b = !fields.contains(f.getName());

                        return a && b;
                    }
                    @Override
                    public boolean shouldSkipClass(Class<?> arg0) {
                        return false;
                    }
                };

                // Rebuild our json
                GsonBuilder gsonBuilder = new GsonBuilder();

                gsonBuilder.setExclusionStrategies(exclusion);
                gstream = gsonBuilder.create();

                // Other options of filtering
            } else if (parameter.equals("watchSiteName")) {
                filterList.add(new FilterPredicate("watchSiteName", FilterOperator.EQUAL, value));
            } else if (parameter.equals("species")) {
                filterList.add(new FilterPredicate("species", FilterOperator.EQUAL, value));
            } else if (parameter.equals("date")) {
                filterList.add(new FilterPredicate("date", FilterOperator.EQUAL, value));
            }
        }

        // I made this filter to avoid the error of 'at least two filters needed
        // ! '
        filterList.add(new FilterPredicate("GUI", FilterOperator.NOT_EQUAL, ""));
        filterList.add(new FilterPredicate("GUI", FilterOperator.NOT_EQUAL, ""));

        // Start filtering
        CompositeFilter filters = new CompositeFilter(CompositeFilterOperator.AND, filterList);

        q.setFilter(filters);

        PreparedQuery      pq                 = dataBase.prepare(q);
        List<Birdwatching> containerBirdWatch = new LinkedList<>();

        // process result
        for (Entity ent : pq.asIterable()) {
            containerBirdWatch.add(processEntityToWatching(ent));
        }

        if (containerBirdWatch.size() != 0) {
            json = gstream.toJson(containerBirdWatch);
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write(json);
        } else {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
