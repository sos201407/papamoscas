package sos.us.es;

public interface Bird {
    public abstract String getId();

    public abstract void setId(String specie);

    public abstract String getPlace();

    public abstract void setPlace(String place);

    public abstract Float getLegDiameter();

    public abstract void setLegDiameter(Float legDiameter);

    public abstract Float getWingSize();

    public abstract void setWingSize(Float wingSize);

    public abstract Integer getEggs();

    public abstract void setEggs(Integer eggs);

    public abstract Integer getEclosions();

    public abstract void setEclosions(Integer eclosions);

    public abstract String getSpecie();

    public void setSpecie(String id);
}


//~ Formatted by Jindent --- http://www.jindent.com
