package sos.us.es;

public class BirdwatchingImpl implements Birdwatching {
    String GUI;
    String species;
    String commonName;
    String watchSiteName;
    Double latitude;
    Double longitude;
    String date;

    public BirdwatchingImpl() {
        super();
    }

    public BirdwatchingImpl(String GUI, String species, String commonName, String watchSiteName, Double latitude,
                            Double longitude, String date) {
        super();
        this.GUI           = GUI;
        this.species       = species;
        this.commonName    = commonName;
        this.watchSiteName = watchSiteName;
        this.latitude      = latitude;
        this.longitude     = longitude;
        this.date          = date;
    }

    @Override
    public String getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(String species) {
        this.species = species;
    }

    @Override
    public String getCommonName() {
        return commonName;
    }

    @Override
    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    @Override
    public String getwatchSiteName() {
        return watchSiteName;
    }

    @Override
    public void setwatchSiteName(String watchSiteName) {
        this.watchSiteName = watchSiteName;
    }

    @Override
    public Double getLatitude() {
        return latitude;
    }

    @Override
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Override
    public Double getLongitude() {
        return longitude;
    }

    @Override
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String getGUI() {
        return GUI;
    }

    @Override
    public void setGUI(String GUI) {
        this.GUI = GUI;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
