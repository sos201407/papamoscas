package sos.us.es;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class Servlet_0 extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/HTML");
        resp.getWriter().println("<html><body><h1>Hello Get!</h1></body></html>");

        String u = req.getParameter("user");
        String p = req.getParameter("pass");

        resp.getWriter().println("<p>" + u + "</p>");
        resp.getWriter().println("<p>" + p + "</p>");
    }

    @Override
    public void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/HTML");
        resp.getWriter().println("<html><body><h1>Hello Put!</h1></body></html>");
    }

    @Override
    public void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/HTML");
        resp.getWriter().println("<html><body><h1>Hello Delete!</h1></body></html>");
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/HTML");
        resp.getWriter().println("<html><body><h1>Hello Post!</h1></body></html>");
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
