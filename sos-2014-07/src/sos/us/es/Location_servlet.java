package sos.us.es;

//~--- non-JDK imports --------------------------------------------------------

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.IOException;

import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class Location_servlet extends HttpServlet {
    public static Gson             gstream  = new Gson();
    public static DatastoreService dataBase = DatastoreServiceFactory.getDatastoreService();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        resp.addHeader("Access-Control-Allow-Origin", "*");
        
        // Default vars
        List<Filter> filterList = new LinkedList<Filter>();
        String       json       = null;
        List<String> uriItems   = null;

        try {
            String resourcePath = req.getPathInfo();

            // Check if the resource path is null. If not extract uri items
            if (resourcePath != null) {
                String[] uri = resourcePath.split("/");

                uriItems = Arrays.asList(uri);
            }

            // If there are not arguments, GET ALL
            if ((resourcePath == null) || (uriItems.size() < 2)) {

                // CheckParams Before
                // If there are not params, return All
                if (req.getParameterNames() == null) {
                    Query          q                  = new Query("location");
                    PreparedQuery  pq                 = dataBase.prepare(q);
                    List<Location> containerLocations = new LinkedList<>();

                    // Get elements as entity with Iterable
                    for (Entity ent : pq.asIterable()) {
                        containerLocations.add(processEntityToLocation(ent));
                    }

                    // Check if exist results. TODO add else with SC_NOT_FOUND
                    if (containerLocations.size() > 0) {
                        json = gstream.toJson(containerLocations);
                        resp.setStatus(HttpServletResponse.SC_OK);
                        resp.setContentType("application/json");
                        resp.setCharacterEncoding("UTF-8");
                        resp.getWriter().write(json);
                    }
                } else {

                    // If there are params
                    doGetWithParameters(req, resp, filterList);
                }
            } else if ((resourcePath != null) && (uriItems.size() > 2)) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }

            // Get ONE With Params ( by default )
            else {
                String id = uriItems.get(1);

                filterList.add(new FilterPredicate("id", FilterOperator.EQUAL, id));
                doGetWithParameters(req, resp, filterList);
            }
        } catch (Exception e) {
            System.out.println("Error ! " + e.getMessage());
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        // setUp();
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");

        List<String> uriItems     = null;
        String       resourcePath = req.getPathInfo();

        // Check if the url is null ( api/v1/ )
        if (resourcePath != null) {
            String[] uri = resourcePath.split("/");

            uriItems = Arrays.asList(uri);
        }

        // Check if there single or multiple vars. Forbidden in PUST
        if ((uriItems == null) || (uriItems.size() < 2) || (uriItems.size() > 2)) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {

            // Try query
            try {
                String id = uriItems.get(1);

                System.out.println("Requested id :" + id);

                Query q = new Query("location").setFilter(new FilterPredicate("id", Query.FilterOperator.EQUAL, id));

                System.out.print(q);

                PreparedQuery pq = dataBase.prepare(q);

                System.out.print(pq);

                Entity ent = pq.asSingleEntity();

                System.out.println("Entity:" + ent);

                // Check if the object exists and if is the right one
                if ((ent != null) && ent.getProperty("id").equals(id)) {
                    dataBase.delete(ent.getKey());

                    Location locationTemp = jsonToWatching(req);
                    Entity   entUpdate    = processLocationToEntity(locationTemp);

                    dataBase.put(entUpdate);
                    resp.setStatus(HttpServletResponse.SC_OK);
                } else {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            } catch (Exception e) {
                System.out.println("Error ! " + e.getMessage());
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
    }

    @Override
    public void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        gstream = new Gson();

        try {
            String resourcePath = req.getPathInfo();

            // check if it's a collection or a single resource
            if ((resourcePath != null) &&!resourcePath.equals("/")) {
                String[] resources = resourcePath.split("/");

                // it only accepts URI's like: /birds/birdID
                if (resources.length > 2) {
                    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                } else {
                    String        id = resources[1];
                    Query         q  = new Query("location").setFilter(new FilterPredicate("id", FilterOperator.EQUAL,
                                           id));
                    PreparedQuery pq = dataBase.prepare(q);

                    // check if query has data to show
                    try {
                        Entity e = pq.asSingleEntity();

                        if (e != null) {

                            // as we use birdId as identifier, we could also
                            // delete by using URI parameter
                            dataBase.delete(e.getKey());
                            resp.setStatus(HttpServletResponse.SC_NO_CONTENT);    // ok
                        } else {
                            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);     // hide

                            // others
                            // not-catched
                            // problems
                        }
                    } catch (Exception e) {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }
                }
            } else {                                                              // it's the entire collection
                Query         q  = new Query("location");
                PreparedQuery pq = dataBase.prepare(q);

                // exceptions here are not being catched beacause it only throws
                // server derivated errors (caught below)
                for (Entity e : pq.asIterable()) {
                    dataBase.delete(e.getKey());
                }

                resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");

        List<String> uriItems     = null;
        String       resourcePath = req.getPathInfo();

        // Check if the url is null ( api/v1/ )
        if (resourcePath != null) {
            String[] uri = resourcePath.split("/");

            uriItems = Arrays.asList(uri);
        }

        // Check if there are vars. Forbidden in POST
        if ((uriItems != null) && (uriItems.size() > 1)) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            try {
                Location locationTemp = jsonToWatching(req);

                if (testElementExists(locationTemp)) {
                    resp.setStatus(HttpServletResponse.SC_CONFLICT);
                    System.out.println("Warning: An element in database already matches this ID ");

                    // throw new
                    // IllegalWriteException("Warning: An element in database already matches this ID ");
                } else {
                    Entity ent = processLocationToEntity(locationTemp);

                    dataBase.put(ent);
                    resp.setStatus(HttpServletResponse.SC_CREATED);
                }
            } catch (Exception e) {
                System.out.println("Error ! " + e.getMessage());
                resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }
    }

    private Boolean testElementExists(Location location) {
        Query q = new Query("location").setFilter(new FilterPredicate("id", Query.FilterOperator.EQUAL,
                      location.getID()));
        PreparedQuery pq2 = dataBase.prepare(q);

        return pq2.asIterable().iterator().hasNext();
    }

    private Location jsonToWatching(HttpServletRequest req) throws IOException {
        StringBuilder  sb         = new StringBuilder();
        BufferedReader br         = req.getReader();
        String         jsonString = sb.toString();

        while ((jsonString = br.readLine()) != null) {
            sb.append(jsonString);
        }

        jsonString = sb.toString();

        Location loc = gstream.fromJson(jsonString, LocationImpl.class);

        return loc;
    }

    private Entity processLocationToEntity(Location location) {
        Entity e = new Entity("location", location.getID());

        e.setProperty("id", location.getID());
        e.setProperty("date", location.getDate());
        e.setProperty("nvdi", location.getNVDI());
        e.setProperty("place", location.getPlace());
        e.setProperty("temp", location.getTemp());

        return e;
    }

    private Location processEntityToLocation(Entity e) {
        String id    = (String) e.getProperty("id");
        String date  = (String) e.getProperty("date");
        Float  nvdi  = new Float(e.getProperty("nvdi").toString());
        String place = (String) e.getProperty("place");
        Float  temp  = new Float(e.getProperty("temp").toString());

        return new LocationImpl(id, date, place, nvdi, temp);
    }

    private void doGetWithParameters(HttpServletRequest req, HttpServletResponse resp, List<Filter> filterList)
            throws IOException {
        String                                             json   = "";
        Query                                              q      = new Query("location");
        @SuppressWarnings("unchecked") Enumeration<String> params = req.getParameterNames();
        resp.addHeader("Access-Control-Allow-Origin", "*");
        
        for (String parameter : Collections.list(params)) {

            // Get list of Parameters' name and it values.
            String[] parameterValue = req.getParameterValues(parameter);
            String   value          = Arrays.toString(parameterValue).replace("[", "").replace("]", "");

            // If the parameters is fields, apply custom veiw
            if (parameter.equals("fields")) {

                //
                final List<String> fields    = Lists.newArrayList(value.split(","));
                ExclusionStrategy  exclusion = new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {

                        // if true -> skip field,
                        // so returns true IF f IS NOT
                        // contained in parameters list
                        boolean a = f.getDeclaringClass() == LocationImpl.class;
                        boolean b = !fields.contains(f.getName());

                        return a && b;
                    }
                    @Override
                    public boolean shouldSkipClass(Class<?> arg0) {
                        return false;
                    }
                };

                // Rebuild our json
                GsonBuilder gsonBuilder = new GsonBuilder();

                gsonBuilder.setExclusionStrategies(exclusion);
                gstream = gsonBuilder.create();

                // Other options of filtering
            } else if (parameter.equals("place")) {
                filterList.add(new FilterPredicate("place", FilterOperator.EQUAL, value));
            } else if (parameter.equals("species")) {
                filterList.add(new FilterPredicate("nvdi", FilterOperator.EQUAL, value));
            } else if (parameter.equals("temp")) {
                filterList.add(new FilterPredicate("temp", FilterOperator.EQUAL, value));
            } else if (parameter.equals("id")) {
                filterList.add(new FilterPredicate("id", FilterOperator.EQUAL, value));
            }
        }

        // I made this filter to avoid the error of 'at least two filters needed
        // ! '
        filterList.add(new FilterPredicate("id", FilterOperator.NOT_EQUAL, ""));
        filterList.add(new FilterPredicate("id", FilterOperator.NOT_EQUAL, ""));

        // Start filtering
        CompositeFilter filters = new CompositeFilter(CompositeFilterOperator.AND, filterList);

        q.setFilter(filters);

        PreparedQuery  pq                = dataBase.prepare(q);
        List<Location> containerLocation = new LinkedList<>();

        // process result
        for (Entity ent : pq.asIterable()) {
            containerLocation.add(processEntityToLocation(ent));
        }

        if (containerLocation.size() != 0) {
            json = gstream.toJson(containerLocation);
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write(json);
        } else {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
