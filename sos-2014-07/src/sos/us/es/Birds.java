package sos.us.es;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

public interface Birds {
    public abstract List<Bird> getBirds();

    public abstract void setBirds(List<Bird> birds);
}


//~ Formatted by Jindent --- http://www.jindent.com
