package sos.us.es;

public class LocationImpl implements Location {
    String id;
    String place;
    Float  nvdi;
    Float  temp;
    String date;

    /**
     * @param id
     * @param date
     * @param place
     * @param nvdi
     * @param temp
     */
    public LocationImpl() {
        super();
    }

    public LocationImpl(String id, String date, String place, Float nvdi, Float temp) {
        super();
        this.id    = id;
        this.date  = date;
        this.place = place;
        this.nvdi  = nvdi;
        this.temp  = temp;
    }

    @Override
    public void setPlace(String place) {
        place = this.place;
    }

    @Override
    public String getPlace() {
        return place;
    }

    @Override
    public void setNVDI(Float nvdi) {
        nvdi = this.nvdi;
    }

    @Override
    public Float getNVDI() {
        return nvdi;
    }

    @Override
    public void setTemp(Float temp) {
        temp = this.temp;
    }

    @Override
    public Float getTemp() {
        return temp;
    }

    @Override
    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    @Override
    public String getID() {
        return id;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
