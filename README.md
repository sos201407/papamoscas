sos-2014-07
===========

###Proyecto para Sistemas Orientados a Servicios (3º)
####Estudio de impactos ambientales y productivos sobre las aves
===========

####Integrantes:

  - Antonio Gámez Díaz
  - Álvaro Rabadán González




####Descripción del proyecto:

Con este estudio se pretenden estudiar las relaciones existentes entre ciertas aves y otros factores ambientales y de producción agrícola.

Este proyecto pretende realizar un análisis entre diferentes fuentes de datos a fin de obtener conclusiones acerca de la relación que existe entre las aves (en su mayoría, migratorias) y otros factores medioambientales y de producción agrícola. No tenemos aún perfectamente definidas las fuentes de información, pues estamos intentando llegar a acuerdos con la Estación Biológica de Doñana y el Instituto de Estadística y Cartografía de Andalucía.


####URL del proyecto:

http://sos-2014-07.appspot.com/
